<div class="modal fade" id="addExpirationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New System Expiration</h4>
                <br/>
                <p class="text-danger">*Notifications will be sent 90, 60, 30 and 15 days prior to the expiration date.</p>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => [ 'expirations.store' ], 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('vendor', 'Vendor', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('vendor', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('reseller', 'Reseller', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('reseller', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('price', 'Price', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('price', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('expiration_date', 'Expiration Date', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('expiration_date', null, ['class' => 'form-control', 'id' => 'datetimepicker']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('recurring', '0') !!}
                    {!! Form::checkbox('recurring', 1, false) !!}
                    {!! Form::label('recurring', 'Recurring (The event will regenerate on the day it expires)') !!}
                </div>
                <div class="form-group">
                    {!! Form::label('team_id', 'Team Notification', ['class' => 'col-sm-4 control-label']) !!}
                    {!! Form::select('team_id', $teams, null, [ 'class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Notifications']) !!}
                </div>
                {!! Form::hidden('created_by', Auth::user()->username) !!}
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>
    <!-- javascript for init -->
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });
</script>
@endpush