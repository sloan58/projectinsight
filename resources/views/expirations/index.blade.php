@extends('layouts.app')

@section('content')

    <expirations-table></expirations-table>

@endsection

@include('expirations.modal.add_expiration')

@section('scripts')

    @stack('modal_scripts')

@endsection