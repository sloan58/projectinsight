@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">System Expirations Calendar</li>
                    </ol>
                    <div class="card">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <a class="navbar-brand" href="#">System Expirations</a>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav navbar-right pull-right">
                                            <button type="button" class="btn btn-success btn-wd bootstrap-modal-form-open" data-toggle="modal" data-target="#addExpirationModal">
                                                <span class="btn-label">
                                                    <i class="fa fa-plus"></i>
                                                </span>
                                                <b>Add Expiration Event</b>
                                            </button>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="content">
                            {!! $calendar->calendar() !!}
                        </div><!-- end content-->
                    </div><!--  end card  -->
                </div> <!-- end col-md-12 -->
            </div> <!-- end row -->

        </div>
    </div>
@endsection

@include('expirations.modal.add_expiration')

@section('scripts')


    @stack('modal_scripts')
    {!! $calendar->script() !!}

@endsection