@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('expirations.index') }}">Expirations</a></li>
                        <li class="active">{{ $expiration->name }}</li>
                    </ol>
                    <div class="card">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand" >Edit Expiration: {{ $expiration->name }}</div>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                    </div>
                                </div>
                                <p class="text-danger">*Notifications will be sent 90, 60, 30 and 15 days prior to the expiration date.</p>
                            </nav>
                        </div>
                        <div class="content">
                            <ul role="tablist" class="nav nav-tabs">
                                <li role="presentation" class="active">
                                    <a href="#details" data-toggle="tab">Details</a>
                                </li>
                                <li>
                                    <a href="#attachments" data-toggle="tab">Attachments</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div id="details" class="tab-pane active">
                                    <div class="toolbar">
                                        {!! Form::model($expiration, ['route' => ['expirations.update', $expiration->id], 'method' => 'PUT', 'id' => 'editExpirationForm', 'class' => 'bootstrap-modal-form']) !!}
                                        <div class="form-group">
                                            {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::text('name', null, ['class' => 'form-control','id' => 'expiration_name']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::textarea('description', null, ['class' => 'form-control','id' => 'expiration_description']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('vendor', 'Vendor', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::text('vendor', null, ['class' => 'form-control', 'id' => 'expiration_vendor']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('reseller', 'Reseller', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::text('reseller', null, ['class' => 'form-control']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('price', 'Price', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::text('price', null, ['class' => 'form-control', 'id' => 'expiration_price']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('expiration_date', 'Expiration Date', ['class' => 'col-sm-3 control-label']) !!}
                                            {!! Form::text('expiration_date', $expiration->expiration_date->toDateString(), ['class' => 'form-control', 'id' => 'datetimepicker']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::hidden('recurring', '0') !!}
                                            {!! Form::checkbox('recurring', 1) !!}
                                            {!! Form::label('recurring', 'Recurring (The event will regenerate on the day it expires)') !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('team_id', 'Team Notification', ['class' => 'col-sm-4 control-label']) !!}
                                            {!! Form::select('team_id', $teams, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Notifications', 'id' => 'team_notification']) !!}
                                        </div>
                                        {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <div id="attachments" class="tab-pane">
                                    <div class="fresh-datatables">
                                        <table id="attachmentsTable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                {{--<th class="disabled-sorting text-center">Actions</th>--}}
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th>Name</th>
                                                {{--<th class="text-center">Actions</th>--}}
                                            </tr>
                                            </tfoot>
                                            <tbody>
                                            @if($expiration->attachments)
                                            @foreach(json_decode($expiration->attachments) as $item)
                                                <tr>
                                                    <td><a href="{{ route('expirations.attachment.download', [ 'expiration' => $expiration->id, 'fileName' => $item]) }}">{{ $item }}</a></td>
                                                    {{--<td class="text-center">--}}
                                                        {{--<a href="{{ route('expirations.attachment.remove', ['expiration' => $expiration->id]) }}"--}}
                                                           {{--rel="tooltip"--}}
                                                           {{--class="btn btn-simple btn-danger btn-icon remove"--}}
                                                           {{--data-method="post"--}}
                                                           {{--data-remote="false"--}}
                                                           {{--data-params="attachment={{ $item }}"--}}
                                                           {{--data-confirm="Are you sure you want to remove {{ $item }}?">--}}
                                                            {{--<i class="fa fa-times"></i>--}}
                                                        {{--</a>--}}
                                                    {{--</td>--}}
                                                </tr>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                    <my-dropzone url="/expirations/{{ $expiration->id }}/add-attachment"></my-dropzone>
                                </div>
                            </div>
                        </div><!-- end content-->
                    </div><!--  end card  -->
                </div> <!-- end col-md-12 -->
            </div> <!-- end row -->
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        <!-- javascript for init -->
        $('#datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            widgetPositioning: {
                horizontal: 'left',
                vertical: 'bottom'
            },
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    </script>

@endsection