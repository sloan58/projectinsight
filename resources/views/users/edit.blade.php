@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('users.index') }}">Users</a></li>
                    <li class="active">{{ $user->username }}</li>
                </ol>
                <div class="col-md-6">
                    <div class="item card card-user">
                        <div class="image" >
                            <img src="{{ asset('img/full-screen-tech.jpg') }}" alt="...">
                        </div>
                        <div class="content">
                            <div class="author">
                                <a href="#">
                                    <img class="avatar border-gray" src="{{ Gravatar::src( $user->email ) }}" alt="...">

                                    <h4 class="title">{{ $user->username }}<br>
                                        <small>{{ $user->team->name or '' }}</small>
                                    </h4>
                                </a>
                            </div>
                            {!! Form::model($user, [
                                    'method' => 'PATCH',
                                    'url' => ['/users', $user->id],
                                ]) !!}
                            @if($user->local_account)
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('email', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                                {!! Form::label('username', 'Username', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('username', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                            </div>
                            @else
                            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                                {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('email', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('username') ? 'has-error' : ''}}">
                                {!! Form::label('username', 'Username', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('username', null, ['class' => 'form-control', 'readonly' => 'readonly']) !!}
                                {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
                            </div>
                            @endif
                            @if(\Auth::user()->hasRole('manager') || \Auth::user()->hasRole('admin'))
                            <div class="form-group {{ $errors->has('roles') ? 'has-error' : ''}}">
                                {!! Form::label('roles', 'Roles', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::select('roles[]', $roles, $user->roles->pluck('id')->toArray(), ['class' => 'form-control selectpicker', 'multiple'=> 'multiple', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue']) !!}
                                {!! $errors->first('roles', '<p class="help-block">:message</p>') !!}
                            </div>
                            @if($user->team)
                            <div class="form-group {{ $errors->has('team_id') ? 'has-error' : ''}}">
                                {!! Form::label('team_id', 'Team Membership', ['class' => 'col-sm-6 control-label']) !!}
                                {!! Form::select('team_id', $teams, $user->team->id, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue']) !!}
                                {!! $errors->first('team_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            @else
                            <div class="form-group {{ $errors->has('team_id') ? 'has-error' : ''}}">
                                {!! Form::label('team_id', 'Team Membership', ['class' => 'col-sm-6 control-label']) !!}
                                {!! Form::select('team_id', $teams, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Assign Team']) !!}
                                {!! $errors->first('team_id', '<p class="help-block">:message</p>') !!}
                            </div>
                            @endif
                            @endif
                            @if($user->hasRole('manager'))
                                <div class="form-group {{ $errors->has('team_management') ? 'has-error' : ''}}">
                                    {!! Form::label('team_management', 'Manages Teams', ['class' => 'col-sm-6 control-label']) !!}
                                    {!! Form::select('team_management[]', $teams, $user->managedTeams->pluck('id')->toArray(), ['class' => 'form-control selectpicker', 'multiple'=> 'multiple', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue']) !!}
                                    {!! $errors->first('team', '<p class="help-block">:message</p>') !!}
                                </div>
                            @endif
                            {{--@if($user->local_account)--}}
                            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                                {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::password('password', ['class' => 'form-control']) !!}
                                {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                            </div>
                            {{--@endif--}}
                            {!! Form::submit('Update', ['class' => 'btn btn-fill btn-info']) !!}
                            {!! Form::close() !!}
                            @if(!Auth::user()->isImpersonating() && Auth::user()->hasRole('admin') && (Auth::user()->id != $user->id))
                            <div class="footer text-center">
                                <a href="{{ url('users/impersonate', [$user->id]) }}">
                                    <button type="submit" class="btn btn-fill btn-success btn-wd">Impersonate {{ $user->username }}</button>
                                </a>
                            </div>
                            @endif
                        </div>
                        <hr>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="item card">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <a class="navbar-brand" href="#">Comp Time Available: <b>{{ $user->availableCompHours() }} Hours</b></a>
                                    </div>
                                    @if($user->id == Auth::user()->id)
                                        <div class="collapse navbar-collapse">
                                            <ul class="nav navbar-nav navbar-right pull-right">
                                                <button type="button" class="btn btn-danger btn-wd bootstrap-modal-form-open" data-toggle="modal" data-target="#useCompTimeModal">
                                                        <span class="btn-label">
                                                            <i class="fa fa-minus"></i>
                                                        </span>
                                                    <b>Use Comp Time</b>
                                                </button>
                                                {{--<button type="button" class="btn btn-success btn-wd bootstrap-modal-form-open" data-toggle="modal" data-target="#addCompTimeModal">--}}
                                                        {{--<span class="btn-label">--}}
                                                            {{--<i class="fa fa-plus"></i>--}}
                                                        {{--</span>--}}
                                                    {{--<b>Add Comp Time</b>--}}
                                                {{--</button>--}}
                                            </ul>
                                        </div>
                                    @endif
                                </div>
                            </nav>
                        </div>
                        <div class="content">
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="fresh-datatables">
                                <table id="compHoursTable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Reason</th>
                                        <th>Hours Added</th>
                                        <th>Project</th>
                                        <th>Hours Redeemed</th>
                                        <th>Submitted On</th>
                                        @if($user->id == Auth::user()->id)
                                        <th class="disabled-sorting text-center">Actions</th>
                                        @endif
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Reason</th>
                                        <th>Hours Added</th>
                                        <th>Project</th>
                                        <th>Hours Redeemed</th>
                                        <th>Submitted On</th>
                                        @if($user->id == Auth::user()->id)
                                        <th class="text-center">Actions</th>
                                        @endif
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    {{-- */$x=0;/* --}}
                                    @foreach($user->compHours as $item)
                                        {{-- */$x++;/* --}}
                                        <tr>
                                            <td>{{ $item->reason }}</td>
                                            @if($item->hours)
                                                <td>{{ $item->hours }}</td>
                                                <td>{{ $item->project->name or ''}}</td>
                                            @else
                                                <td></td>
                                                <td></td>
                                            @endif
                                            @if($item->redeemed_hours)
                                                <td>{{ $item->redeemed_hours }}</td>
                                            @else
                                                <td></td>
                                            @endif
                                            <td>{{ $item->created_at->toDateString() }}</td>
                                            @if($user->id == Auth::user()->id)
                                                <td class="td-actions">
                                                    <a href="{{ route('comp-hours.destroy', $item->id) }}"
                                                       rel="tooltip"
                                                       class="btn btn-simple btn-danger btn-icon"
                                                       data-method="delete"
                                                       data-remote="false"
                                                       data-confirm="Are you sure you want to delete {{ $item->description }}?">
                                                        <i class="fa fa-remove"></i>
                                                    </a>
                                                </td>
                                            @endif
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end content-->
                    </div><!--  end card  -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <pi-line-graph chart-id="individualHoursThisWeekChart" url="/api/charts/user-profile/hours/this-week/{{ $user->id }}" chart-div-size="col-md-12"></pi-line-graph>
                    <pi-line-graph chart-id="individualHoursAllWeeksChart" url="/api/charts/user-profile/hours/all-weeks/{{ $user->id }}" chart-div-size="col-md-12"></pi-line-graph>
                </div>
                <div class="col-md-6">
                    <pi-pie-graph chart-id="myTaskOpenClosedChart2" url="/api/charts/user-profile/tasks/{{ $user->id }}" chart-div-size="col-md-12"></pi-pie-graph>
                </div>
            </div>
        </div>
    </div>
@stop

@include('users.modals.add_comp_time')
@include('users.modals.use_comp_time')

@section('scripts')
    @include('partials._datatables', ['name' => 'compHoursTable', 'sort' => 4, 'direction' => 'desc'])

    <script>
        $(function() {
            $('.item').matchHeight();
        });

    </script>
@endsection

