@extends('layouts.app')

@section('content')

    <users-table></users-table>

@endsection

@include('users.modals.add_user')

@section('scripts')

    @stack('modal_scripts')

@endsection