<div class="modal fade" id="addCompTimeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Add Comp Time</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/comp-hours', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('reason', 'Reason', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('reason', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('hours', 'Hours', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('hours', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('project_id', 'Project', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('project_id', $projects, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Project']) !!}
                </div>
                {!! Form::hidden('user_id', Auth::user()->id) !!}
                {!! Form::submit('Create', ['class' => 'btn btn-fill btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>