<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New User</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/users', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('username', 'Username', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('username', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('email', 'Email', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('roles', 'Roles', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('roles[]', $roles, null, ['class' => 'form-control selectpicker', 'multiple'=> 'multiple', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Roles']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('team_id', 'Team Membership', ['class' => 'col-sm-6 control-label']) !!}
                    {!! Form::select('team_id', $teams, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select a Team']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('password', 'Password', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                </div>
                {!! Form::submit('Create', ['class' => 'btn btn-fill btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>