<div class="modal fade" id="editStatusModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Status Update</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'PUT', 'id' => 'editStatusForm', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control', 'id' => 'status_title_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('body', 'Body', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('body', null, ['class' => 'form-control', 'id' => 'status_body_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('hours_spent', 'Hours Spent', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('hours_spent', null, ['class' => 'form-control', 'id' => 'status_hours_spent_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('performed_at', 'Work Performed On', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('performed_at', null, ['class' => 'form-control', 'id' => 'datetimepicker_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('admin_time', 0) !!}
                    {!! Form::label('admin_time', 'Was this Admin Time?') !!}
                    {{ Form::checkbox('admin_time', 1, null, ['id' => 'status_admin_time_edit']) }}
                </div>
                {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>

    <!-- javascript for init -->
    $('#datetimepicker_edit').datetimepicker({
        format: 'YYYY-MM-DD',
        maxDate: new Date(),
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    var editStatusModal = $('#editStatusModal');

    //triggered when modal is about to be shown
    editStatusModal.on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        projectId = $(e.relatedTarget).data('project_id');
        taskId = $(e.relatedTarget).data('task_id');
        statusId = $(e.relatedTarget).data('status_id');
        statusTitle = $(e.relatedTarget).data('status_title');
        statusBody = $(e.relatedTarget).data('status_body');
        statusTask = $(e.relatedTarget).data('status_task');
        statusHoursSpent = $(e.relatedTarget).data('status_hours_spent');
        statusPerformedAt = $(e.relatedTarget).data('status_performed_at');
        statusAdminTime = $(e.relatedTarget).data('status_admin_time');

        // Set the Modal values with the data attributes
        $("#status_title_edit").val(statusTitle);
        $("#status_body_edit").html(statusBody);
        $("#status_hours_spent_edit").val(statusHoursSpent);
        $("#task_id_edit").val(taskId);
        if(statusAdminTime) {
            console.log(statusAdminTime);
            $("#status_admin_time_edit").prop("checked", true);
        }

        // Plug in the form endpoint
        $("#editStatusForm").attr('action', '/statuses/' + statusId);

        $('#datetimepicker_edit').val(statusPerformedAt).datetimepicker('update');

    });

    //triggered when modal is about to be hiden
    editStatusModal.on('hide.bs.modal', function(e) {

        // Uncheck admin time on modal hide
        $("#status_admin_time_edit").prop("checked", false);

        $('#datetimepicker_edit').val('').datetimepicker('update');

    });

</script>
@endpush