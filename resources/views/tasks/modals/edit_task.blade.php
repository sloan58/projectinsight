<div class="modal fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Task</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'PUT', 'id' => 'editTaskForm', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Task Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'task_name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'task_description']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('assigned_to', 'Assignment', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('assigned_to', $task->project->team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'task_selected_user_id']) !!}
                </div>
                {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>
    //triggered when modal is about to be shown
    $('#editTaskModal').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var taskName = $(e.relatedTarget).data('task_name');
        var taskDescription = $(e.relatedTarget).data('task_description');
        var taskId = $(e.relatedTarget).data('task_id');
        var taskUserName = $(e.relatedTarget).data('task_user_name');

        // Find the span from the select2 dropdown list where the name equals
        // that of the assigned user.  Set the class to 'selected'
        $("span:contains(" + taskUserName + ")").closest('li').prop('class', 'selected');

        // Set the select2 display to the username of the assigned User
        $("button[data-id=task_selected_user_id] span:first").text(taskUserName);
        // Set the button title equal to the name of the selected user
        $("button[data-id=task_selected_user_id]").prop('title', taskUserName);
        // Set the actual HTML select option for the assigned user to 'selected'
        $("select#task_selected_user_id option:contains(" + taskUserName + ")").prop('selected', 'selected');
        // Set the Modal values with the data attributes
        $("#task_name").val(taskName);
        $("#task_description").html(taskDescription);

        // Plug in the form endpoint
        $("#editTaskForm").attr('action', '/tasks/' + taskId);
    });
</script>
@endpush