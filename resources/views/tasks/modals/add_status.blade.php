<!-- Add Status Update Modal -->
<div class="modal fade" id="addStatusUpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Status Update</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => ['statuses.store'], 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('title', 'Title', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('title', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('body', 'Body', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('body', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('hours_spent', 'Hours Spent', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('hours_spent', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('performed_at', 'Work Performed On', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('performed_at', Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'id' => 'datetimepicker']) !!}
                </div>
                <div class="form-group">
                    {!! Form::hidden('admin_time', 0) !!}
                    {!! Form::label('admin_time', 'Was this Admin Time?') !!}
                    {!! Form::checkbox('admin_time', 1 , null, ['id' => 'status_admin_time']) !!}
                </div>
                {!! Form::hidden('task_id', $task->id) !!}
                {!! Form::hidden('user_id', Auth::user()->id) !!}
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
<!-- End Add Status Update Modal -->

@push('modal_scripts')
<script>

    //triggered when modal is about to be shown
    $('#addStatusUpdateModal').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        taskId = $(e.relatedTarget).data('task_id');

        console.log(taskId);

        // Set the Modal values with the data attributes
        $("#task_id").val(taskId);

    });

    <!-- javascript for init -->
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

</script>
@endpush