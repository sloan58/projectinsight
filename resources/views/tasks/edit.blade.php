@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li><a href="{{ route('projects.index') }}">Projects</a></li>
                        <li><a href="{{ route('projects.edit', ['project' => $task->project->id]) }}">{{ $task->project->name }}</a></li>
                        <li class="active">{{ $task->name }}</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="header">
                            Edit Task
                        </div>
                        <div class="content">
                            {!! Form::model( $task, ['method' => 'PUT', 'id' => 'editTaskForm', 'route' => ['tasks.update', $task->id]]) !!}
                            <div class="form-group">
                                {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'task_name']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'task_description']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('assigned_to', 'Assignment', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::select('assigned_to', $task->project->team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'task_selected_user_id']) !!}
                            </div>
                            {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                        </div>
                        <div class="content">
                            <pi-status-timeline :task="{{ $task->toJson() }}" :user="{{ Auth::user()->toJson() }}"></pi-status-timeline>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')


@endsection