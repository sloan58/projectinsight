@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div id="vue-graphs">
                <div class="row">
                    {{-- ChartJS Staff Hours Week To Date --}}
                    <pi-bar-graph chart-id="staffHoursWtdChart" url="/api/charts/reporting/staff/wtd" chart-div-size="col-md-12"></pi-bar-graph>
                </div>
                <div class="row">
                    {{-- ChartJS Staff Hours Last Seven Days --}}
                    <pi-bar-graph chart-id="staffHoursLast7Chart" url="/api/charts/reporting/staff/last7" chart-div-size="col-md-12"></pi-bar-graph>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection