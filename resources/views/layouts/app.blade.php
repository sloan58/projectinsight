<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="#" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>{{ config('app.name') }}</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    {{-- Add Laravel CSRF Tokens--}}
    <meta name="csrf-token" content="<?= csrf_token() ?>" />
    <meta name="csrf-param" content="_token" />

    <!--     Fonts and icons     -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis.css" rel="stylesheet" />
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="//cdn.bootcss.com/pixeden-stroke-7-icon/1.2.3/dist/pe-icon-7-stroke.css" />

</head>
<body class="sidebar-mini">
    <div id="vue-app">
        @if(Auth::guest())
            <div class="wrapper wrapper-full-page">
                <div class="full-page login-page" data-color="orange" data-image="{{ asset('img/full-screen-tech_new.jpg') }}">
                    @yield('content')
                </div>
            </div>
        @else
            <div>
                <div class="wrapper">
                    <div class="sidebar" data-color="{{ Auth::user()->isImpersonating() ? 'red' : 'orange' }}" data-image="{{ asset('img/full-screen-tech_new.jpg') }}">

                        @include('partials._sidebar')

                    </div>
                    <div class="main-panel">

                        @include('partials._top_nav')

                        @yield('content')

                    </div>
                </div>
            </div>

        @endif
    </div>

    <!--   Main Application Combined JS   -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <script src="/js/object-assign.min.js"></script>
    <script>
        // Polyfill, modifying the global Object
        window.ObjectAssign.polyfill();
    </script>
    <script src="{{ asset('/js/app.js') }}"></script>
    @include('partials._notifications')
    @yield('scripts')
</body>
</html>
