@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('teams.index') }}">Teams</a></li>
                    <li class="active">{{ $team->name }}</li>
                </ol>
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit Team</h4>
                        </div>
                        <div class="content">
                            {!! Form::model($team, [
                                    'method' => 'PATCH',
                                    'url' => ['/teams', $team->id],
                                ]) !!}
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            <div class="form-group {{ $errors->has('direct_manager') ? 'has-error' : ''}}">
                                {!! Form::label('direct_manager', 'Direct Manager', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::select('direct_manager', $team->managers->pluck('username', 'id'), isset($team->directManager) ? $team->directManager->id : null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'placeholder' => 'Select a Team Direct Manager']) !!}
                            </div>
                            <div class="form-group {{ $errors->has('team_lead') ? 'has-error' : ''}}">
                                {!! Form::label('team_lead', 'Team Lead', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::select('team_lead', $team->members->pluck('username', 'id'), isset($team->lead) ? $team->lead->id : null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'placeholder' => 'Select a Team Lead']) !!}
                            </div>
                            {!! Form::submit('Update', ['class' => 'btn btn-fill btn-info']) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <teams-managers-table team-id="{{  $team->id }}"></teams-managers-table>
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Team Services and Roles</h4>
                        </div>
                        <div class="content">
                            {!! Form::open([
                                    'route' => ['teams.specializations.store', $team->id],
                                ]) !!}
                            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                                {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                                {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                            </div>
                            {!! Form::submit('Add', ['class' => 'btn btn-fill btn-info']) !!}
                            {!! Form::close() !!}
                        </div>
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <div class="navbar-header">
                                    <div class="navbar-brand">Current Specializations</div>
                                </div>
                                <div class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right pull-right">
                                    </ul>
                                </div>
                            </div>
                        </nav>
                        <div class="content">
                            <div class="fresh-datatables">
                                <table id="teamMembersTable" class="table table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Contacts</th>
                                        <th>Actions</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($specializations as $item)
                                        <tr>
                                            <td>{{ $item->name }}</td>
                                            <td>{{ $item->description }}</td>
                                            <td>{{ $item->primaryContact->username or '' }}<br>{{ $item->secondaryContact->username or '' }}</td>
                                            <td class="">
                                                <a data-toggle="modal"
                                                   data-target="#editSpecializationModal"
                                                   data-specialization_id="{{ $item->id }}"
                                                   data-specialization_name="{{ $item->name }}"
                                                   data-specialization_description="{{ $item->description }}"
                                                   data-specialization_primary="{{ $item->primaryContact->username or 'Select Assignment' }}"
                                                   data-specialization_secondary="{{ $item->secondaryContact->username or 'Select Assignment' }}"
                                                   rel="tooltip"
                                                   class="btn btn-simple btn-warning btn-icon table-action edit bootstrap-modal-form-open"
                                                   data-original-title="Edit Specialization"
                                                   href="#"><i class="fa fa-edit"></i>
                                                </a>
                                                <a href="/teams/{{$team->id}}/specializations/{{$item->id}}"
                                                   rel="tooltip"
                                                   class="btn btn-simple btn-danger btn-icon remove"
                                                   data-method="delete"
                                                   data-remote="false"
                                                   data-confirm="Are you sure you want to delete {{ $item->name }}?">
                                                    <i class="fa fa-times"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">Team {{ $team->name }} Members</div>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav navbar-right pull-right">
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="content">
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <div class="fresh-datatables">
                                <table id="teamMembersTable" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    @foreach($team->members as $item)
                                        <tr>
                                            <td>{{ $item->username }}</td>
                                            <td>{{ $item->email }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- end content-->
                    </div><!--  end card  -->
                </div> <!-- end col-md-12 -->

            </div>
        </div>
    </div>
@stop

@include('teams.modals.edit_specialization')

@section('scripts')
    @stack('modal_scripts')
@stop