@extends('layouts.app')

@section('content')

    <teams-table></teams-table>

@endsection

@include('teams.modals.add_team')

@section('scripts')

    @stack('modal_scripts')

@endsection