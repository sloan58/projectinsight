<div class="modal fade" id="editSpecializationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Specialization</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'PUT', 'id' => 'editSpecializationForm', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'specialization_name']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'specialization_description']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('primary_contact', 'Primary Contact', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('primary_contact', $team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'specialization_primary']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('secondary_contact', 'Secondary Contact', ['class' => 'col-sm-4 control-label']) !!}
                    {!! Form::select('secondary_contact', $team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'specialization_secondary']) !!}
                </div>
                {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>

    var editStatusModal = $('#editSpecializationModal');

    //triggered when modal is about to be shown
    editStatusModal.on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var specializationName = $(e.relatedTarget).data('specialization_name');
        var specializationDescription = $(e.relatedTarget).data('specialization_description');
        var specializationId = $(e.relatedTarget).data('specialization_id');
        var specializationPrimary = $(e.relatedTarget).data('specialization_primary');
        var specializationSecondary = $(e.relatedTarget).data('specialization_secondary');

        // Find the span from the select2 dropdown list where the name equals
        // that of the data attributes from the table.  Set the class to 'selected'
        $("span:contains(" + specializationPrimary + ")").closest('li').prop('class', 'selected');
        $("span:contains(" + specializationSecondary + ")").closest('li').prop('class', 'selected');
        // Set the select2 display to the correct selected name
        $("button[data-id=specialization_primary] span:first").text(specializationPrimary);
        $("button[data-id=specialization_secondary] span:first").text(specializationSecondary);
        // Set the button title equal to the name of the selected user
        $("button[data-id=specialization_primary]").prop('title', specializationPrimary);
        $("button[data-id=specialization_secondary]").prop('title', specializationSecondary);
        // Set the actual HTML select option for the assigned user to 'selected'
        $("select#specialization_primary option:contains(" + specializationPrimary + ")").prop('selected', 'selected');
        $("select#specialization_secondary option:contains(" + specializationSecondary + ")").prop('selected', 'selected');

        // Set the Modal values with the data attributes
        $("#specialization_name").val(specializationName);
        $("#specialization_description").html(specializationDescription);

        // Plug in the form endpoint
        $("#editSpecializationForm").attr('action', '/teams/{{$team->id}}/specializations/' + specializationId);
    });

</script>
@endpush