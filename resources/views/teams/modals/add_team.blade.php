<!-- Add System Expiration Modal -->
<div class="modal fade" id="addTeamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Team</h4>
                <br/>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => [ 'teams.store' ], 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>