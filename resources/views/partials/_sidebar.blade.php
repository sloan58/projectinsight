<div class="logo">
    <a href="{{ route('home') }}" class="logo-text">
        Project Insight
    </a>
</div>
<div class="logo logo-mini">
    <a href="/" class="logo-text">
        Pi
    </a>
</div>

<div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
            <img src="{{ Gravatar::src( Auth::user()->email ) }}" />
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                {{ Auth::user()->username }}
                <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
                <ul class="nav">
                    <li><a href="{{ route('users.edit', ['id' => Auth::user()->id]) }}">My Profile</a></li>
                </ul>
            </div>
        </div>
    </div>

    <ul class="nav">
        <li @if(in_array(Route::current()->getName(), ['home'])) class="active" @endif>
            <a href="{{ route('home') }}">
                <i class="pe-7s-graph"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li @if(in_array(Route::current()->getName(), ['projects.index', 'projects.edit'])) class="active" @endif>
            <a href="{{ route('projects.index') }}">
                <i class="pe-7s-note2"></i>
                <p>Projects</p>
            </a>
        </li>
        <li @if(in_array(Route::current()->getName(), ['tasks.index', 'tasks.edit'])) class="active" @endif>
            <a href="{{ route('tasks.index') }}">
                <i class="fa fa-tasks-o" aria-hidden="true"></i>
                <p>Tasks</p>
            </a>
        </li>
        <li @if(in_array(Route::current()->getName(), ['expirations.index', 'expirations.calendar'])) class="active" @endif>
            <a data-toggle="collapse" href="#expirations" area-expanded="true" aria-expanded="false" class="collapsed">
                <i class="pe-7s-timer"></i>
                <p>System Expirations
                    <b class="caret"></b>
                </p>
            </a>
            <div @if(in_array(Route::current()->getName(), ['expirations.index', 'expirations.calendar'])) class="collapse" id="expirations" @else class="collapse" id="expirations" aria-expanded="false" @endif>
                <ul class="nav">
                    <li><a href="{{ route('expirations.index') }}">List</a></li>
                </ul>
                <ul class="nav">
                    <li><a href="{{ route('expirations.calendar') }}">Calendar</a></li>
                </ul>
            </div>
        </li>
        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager'))
        <li @if(in_array(Route::current()->getName(), ['reporting.staff'])) class="active" @endif>
            <a data-toggle="collapse" href="#staffReporting" area-expanded="true" aria-expanded="false" class="collapsed">
                <i class="pe-7s-graph2"></i>
                <p>Reporting
                    <b class="caret"></b>
                </p>
            </a>
            <div @if(in_array(Route::current()->getName(), ['reporting.staff'])) class="collapse" id="staffReporting" @else class="collapse" id="staffReporting" aria-expanded="false" @endif>
                <ul class="nav">
                    <li><a href="{{ route('reporting.staff') }}">My Staff</a></li>
                </ul>
            </div>
        </li>
        @endif
        @if(Auth::user()->hasRole('admin') || Auth::user()->hasRole('manager'))
        <li @if(in_array(Route::current()->getName(), ['teams.index', 'teams.edit'])) class="active" @endif>
            <a href="{{ route('teams.index') }}">
                <i class="pe-7s-users"></i>
                <p>Teams</p>
            </a>
        </li>
        <li @if(in_array(Route::current()->getName(), ['users.index', 'users.edit'])) class="active" @endif>
            <a href="{{ route('users.index') }}">
                <i class="pe-7s-id"></i>
                <p>Users</p>
            </a>
        </li>
        @endif
        @if(Auth::user()->isImpersonating())
        <li class="fa-blink">
            <a href="{{ url('users/stop') }}">
                <i class="fa fa-user-secret" aria-hidden="true"></i>
                <p>Stop Impersonating</p>
            </a>
        </li>
        @endif
    </ul>
</div>