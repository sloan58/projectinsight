{{-- Datatables serverSide --}}
<script>
    var table = $('#{{ $name }}').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{{ $api }}',
        info: false,
        bSort: false,
        fnDrawCallback: function() {
            $('.paginate_button').removeClass('paginate_button');
        }
    });


    var filterButton = $("#dataTablesFilterButton");
    var clearFilterButton = $("#dataTablesClearFilterButton");
    filterButton.on('click', function() {
        table.search("{{ Auth::user()->username }}").draw();
        filterButton.toggle();
        clearFilterButton.toggle();
    });
    clearFilterButton.on('click', function() {
        table.search("").draw();
        filterButton.toggle();
        clearFilterButton.toggle();
    });

</script>