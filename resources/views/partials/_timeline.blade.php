<script src="https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis.js"></script>
<script type="text/javascript">
    var items;
    // DOM element where the Timeline will be attached
    var container = document.getElementById('visualization');
    // Create a DataSet (allows two way data-binding)
    $.ajax({
        url: "/api/milestones/{{ Auth::user()->id }}",
        cache: false,
        success: function(data){
            buildTimeline(data);
        }
    });
    function buildTimeline(data) {
        var timelineData = JSON.parse(data);
        items = new vis.DataSet(timelineData);

        // Configuration for the Timeline
        var options = {
            clickToUse: true,
            start: '{{ Carbon\Carbon::now()->subMonth()->toDateString() }}',
            end: '{{ Carbon\Carbon::now()->addMonths(2)->toDateString() }}',  // end is optional
        };

        // Create a Timeline
        var timeline = new vis.Timeline(container, items, options);
    }
</script>