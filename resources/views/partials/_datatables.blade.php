<script type="text/javascript">
    $(document).ready(function() {
        $('#{{ $name }}').DataTable({
            pagingType: "full_numbers",
            order: [[ {{ $sort }}, "{{ $direction }}" ]],
            lengthMenu: [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: {{ $pageLength or 10 }},
            responsive: true,   
            language: {
                search: "_INPUT_",
                searchPlaceholder: "Search records",
            }

        });


        var table = $('#{{ $name }}').DataTable();

        // Filter Tasks table based on current User
        var filterButton = $("#dataTablesFilterButton");
        var clearFilterButton = $("#dataTablesClearFilterButton");

        filterButton.on('click', function() {
            table.search("{{ Auth::user()->username }}").draw();
            filterButton.toggle();
            clearFilterButton.toggle();
        });

        clearFilterButton.on('click', function() {
            table.search("").draw();
            filterButton.toggle();
            clearFilterButton.toggle();
        });

        // Edit record
        table.on( 'click', '.edit', function () {
            $tr = $(this).closest('tr');

            var data = table.row($tr).data();
//                alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
        } );

        // Delete a record
        table.on( 'click', '.remove', function (e) {
            $tr = $(this).closest('tr');
            table.row($tr).remove().draw();
            e.preventDefault();
        } );

        //Like record
        table.on( 'click', '.like', function () {
//            alert('You clicked on Like button');
        });
    });


    // Datatables in hidden panels need to be re-adjusted when shown
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        $('#{{ $name }}').DataTable().columns.adjust().responsive.recalc();
    });

</script>