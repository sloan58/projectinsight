<!-- Modal -->
<div id="report">
    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Project Insight Reporting</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => ['projects.report']]) !!}
                    <div class="form-group" v-show="custom_dates">
                        <label for="start_date" class="col-sm-3 control-label">Start Date</label>
                        <input type="text" class="form-control datetimepicker" name="start_date" placeholder="Report Start Date">
                    </div>
                    <div class="form-group" v-show="custom_dates">
                        <label for="end_date" class="col-sm-3 control-label">End Date</label>
                        <input type="text" class="form-control datetimepicker" name="end_date" placeholder="Report End Date">
                    </div>
                    <div class="form-group {{ $errors->has('all_dates') ? 'has-error' : ''}}">
                        {!! Form::label('all_dates', 'Report on All Dates') !!}
                        <input name="all_dates" type="checkbox" value="" id="all_dates" v-model="all_dates">
                        {!! $errors->first('all_dates', '<p class="help-block">:message</p>') !!}
                    </div>
                    {!! Form::submit('Send Report   ', ['class' => 'btn btn-wd btn-info']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
    <script>
        new Vue({
            el: "#report",
            data: {
                all_dates: true
            },
            computed: {
                custom_dates: function() {
                    return !this.all_dates
                }
            }
        });
    </script>
    <script>
        <!-- javascript for init -->
        $('.datetimepicker').datetimepicker({
            format: 'YYYY-MM-DD',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        });
    </script>
@endpush