<div class="modal fade" id="editMilestoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Milestone</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'PUT', 'id' => 'editMilestoneForm', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'id' => 'milestone_name_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'milestone_description_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('complete_by', 'Complete By', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('complete_by', null, ['class' => 'form-control', 'id' => 'datetimepickerEdit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('assigned_to', 'Assigned To', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('assigned_to', $project->team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'milestone_assigned_to_id']) !!}
                </div>
                {!! Form::submit('Update', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>

    <!-- javascript for init -->
    $('#datetimepickerEdit').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

    var editMilestoneModal = $('#editMilestoneModal');

    //triggered when modal is about to be shown
    editMilestoneModal.on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var milestoneId = $(e.relatedTarget).data('milestone_id');
        var milestoneName = $(e.relatedTarget).data('milestone_name');
        var milestoneDescription = $(e.relatedTarget).data('milestone_description');
        var milestoneCompleteBy = $(e.relatedTarget).data('milestone_complete_by');
        var milestoneAssignedTo = $(e.relatedTarget).data('milestone_assigned_to');

        // Find the span from the select2 dropdown list where the name equals
        // that of the assigned user.  Set the class to 'selected'
        $("span:contains(" + milestoneAssignedTo + ")").closest('li').prop('class', 'selected');

        // Set the select2 display to the username of the assigned User
        $("button[data-id=milestone_assigned_to_id] span:first").text(milestoneAssignedTo);
        // Set the button title equal to the name of the selected user
        $("button[data-id=milestone_assigned_to_id]").prop('title', milestoneAssignedTo);
        // Set the actual HTML select option for the assigned user to 'selected'
        $("select#milestone_assigned_to_id option:contains(" + milestoneAssignedTo + ")").prop('selected', 'selected');

        // Set the Modal values with the data attributes
        $("#milestone_name_edit").val(milestoneName);
        $("#milestone_description_edit").html(milestoneDescription);

        // Plug in the form endpoint
        $("#editMilestoneForm").attr('action', '/milestones/' + milestoneId);

        $('#datetimepickerEdit').val(milestoneCompleteBy).datetimepicker('update');

    });

    //triggered when modal is about to be hiden
    editMilestoneModal.on('hide.bs.modal', function(e) {

//        $('#milestoneDatetimepicker_edit').val('').datetimepicker('update');

    });

</script>
@endpush