<div class="modal fade" id="editProjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Project</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'PUT', 'id' => 'editTaskForm', 'class' => 'bootstrap-modal-form', 'url' => ['/projects', $project->id] ]) !!}
                <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required', 'id' => 'project_name']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'project_description']) !!}
                    {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                </div>
                <div class="form-group {{ $errors->has('team_id') ? 'has-error' : ''}}">
                    {!! Form::label('team_id', 'Assigned Teams', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('team_id', $teams, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment', 'id' => 'team_id']) !!}
                    {!! $errors->first('team_id', '<p class="help-block">:message</p>') !!}
                </div>
                @if($project->is_closed)
                    {!! Form::submit('Update Project', ['class' => 'btn btn-wd btn-info', 'disabled' => 'disabled']) !!}
                @else
                    {!! Form::submit('Update Project', ['class' => 'btn btn-wd btn-info']) !!}
                @endif
                {{--@if(Sentinel::inRole('admin') || Sentinel::inRole('manager'))--}}
                @if($project->is_closed)
                    <a href="{{ route('projects.toggle', ['project' => $project->id]) }}" class="btn btn-success">Reopen Project</a>
                @else
                    <a href="{{ route('projects.toggle', ['project' => $project->id]) }}" class="btn btn-danger">Close Project</a>
                @endif
                {{--@endif--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>
    //triggered when modal is about to be shown
    $('#editProjectModal').on('show.bs.modal', function(e) {

        //get data-id attribute of the clicked element
        var projectName = $(e.relatedTarget).data('project_name');
        var projectDescription = $(e.relatedTarget).data('project_description');
        var projectId = $(e.relatedTarget).data('project_id');
        var projectTeam = $(e.relatedTarget).data('project_team');
        console.log(projectTeam);

        // Find the span from the select2 dropdown list where the name equals
        // that of the assigned user.  Set the class to 'selected'
        $("span:contains(" + projectTeam + ")").closest('li').prop('class', 'selected');
        // Set the select2 display to the username of the assigned User
        $("button[data-id=team_id] span:first").text(projectTeam);
        // Set the button title equal to the name of the selected user
        $("button[data-id=team_id]").prop('title', projectTeam);
        // Set the actual HTML select option for the assigned user to 'selected'
        $("select#team_id option:contains(" + projectTeam + ")").prop('selected', 'selected');


        // Set the Modal values with the data attributes
        $("#project_name").val(projectName);
        $("#project_description").html(projectDescription);

        // Plug in the form endpoint
        $("#editProjectForm").attr('action', '/projects/' + projectId);
    });
</script>
@endpush