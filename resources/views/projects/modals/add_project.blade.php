<div class="modal fade" id="addProjectModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Project</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => '/projects', 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('team_id', 'Teams', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('team_id', $teams, null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'placeholder' => 'Select a Team']) !!}
                </div>
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>