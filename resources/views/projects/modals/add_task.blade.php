<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Task</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => [ 'tasks.store' ], 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Task Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control', 'id' => 'task_description_edit']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('assigned_to', 'Assigned To', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('assigned_to', $project->team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment']) !!}
                </div>
                {!! Form::hidden('created_by', Auth::user()->username) !!}
                {!! Form::hidden('project_id', $project->id) !!}
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>