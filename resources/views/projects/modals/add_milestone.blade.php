<div class="modal fade" id="addMilestoneModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">New Milestone</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['route' => [ 'milestones.store', 'project' => $project->id ], 'class' => 'bootstrap-modal-form']) !!}
                <div class="form-group">
                    {!! Form::label('name', 'Name', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('description', 'Description', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('complete_by', 'Complete By', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::text('complete_by', Carbon\Carbon::now()->format('Y-m-d'), ['class' => 'form-control', 'id' => 'milestoneDatetimepicker']) !!}
                </div>
                <div class="form-group">
                    {!! Form::label('assigned_to', 'Assigned To', ['class' => 'col-sm-3 control-label']) !!}
                    {!! Form::select('assigned_to', $project->team->members->pluck('username', 'id'), null, ['class' => 'form-control selectpicker', 'data-style' => 'btn-info btn-fill btn-block', 'data-menu-style' => 'dropdown-blue', 'data-title' => 'Select Assignment']) !!}
                </div>
                {!! Form::hidden('created_by', Auth::user()->username) !!}
                {!! Form::hidden('project_id', $project->id) !!}
                {!! Form::submit('Create', ['class' => 'btn btn-wd btn-info']) !!}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>

@push('modal_scripts')
<script>

    <!-- javascript for init -->
    $('#milestoneDatetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    });

</script>
@endpush