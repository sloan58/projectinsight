@extends('layouts.app')

@section('content')

    <pi-projects-table can-send-report="{{ Auth::user()->hasRole('manager') }}"></pi-projects-table>

@endsection

@include('projects.modals.add_project')
@include('projects.modals.project_report')

@section('scripts')

    @stack('modal_scripts')

@endsection