@extends('layouts.app')

@section('content')

    <pi-project-edit :project="{{ $project }}"></pi-project-edit>

@endsection

@include('projects.modals.add_task')
@include('projects.modals.add_milestone')
@include('projects.modals.edit_milestone')
@include('projects.modals.edit_project')

@section('scripts')

    @stack('modal_scripts')

@endsection