@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <ol class="breadcrumb">
                        <li><a href="{{ route('home') }}">Home</a></li>
                        <li class="active">Users</li>
                    </ol>
                    <div class="card">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <a class="navbar-brand" href="#">Users</a>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                        <ul class="nav navbar-nav navbar-right pull-right">
                                            <button type="button" class="btn btn-success btn-wd bootstrap-modal-form-open" data-toggle="modal" data-target="#addUserModal">
                                                <span class="btn-label">
                                                    <i class="fa fa-plus"></i>
                                                </span>
                                                <b>Add User</b>
                                            </button>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="content">
                            <div class="toolbar">
                                <!--        Here you can write extra buttons/actions for the toolbar              -->
                            </div>
                            <my-app></my-app>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection