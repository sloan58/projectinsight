@extends('layouts.app')

@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div id="dashboard-jumbo" class="jumbotron">
                        <div class="pull-right">
                            <a href="/2017.02.17_Project-Insight-User-Guide-V2.0.docx" class="btn btn-fill btn-success" download="/2017.02.17_Project-Insight-User-Guide-V2.0.docx">Download the User Guide</a>
                        </div>
                        <div class="col-md-12">
                            <i id="life-ring-green" class="fa fa-4x fa-life-ring"></i><h2>Team Services and Roles</h2>
                        </div>
                        <!-- plain taxt tabs -->
                        <ul role="tablist" class="nav nav-tabs">
                            @foreach($teams as $team)
                            <li role="presentation" class="{{ $loop->iteration == 1 ? 'active' : '' }}">
                                <a href="#{{ $team->id }}" data-toggle="tab">{{ $team->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach($teams as $team)
                            <div id="{{ $team->id }}" class="tab-pane {{ $loop->iteration == 1 ? 'active' : '' }}">
                                <p><b>Manager:</b> {{ $team->directManager->username or '' }}</p>
                                <p><b>Team Lead:</b> {{ $team->lead->username or '' }}</p>
                                <div class="fresh-datatables">
                                    <table id="{{ $team->id }}_Table" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                        <thead>
                                        <tr>
                                            <th>Technology</th>
                                            <th>Description</th>
                                            <th>Primary Contact</th>
                                            <th>Secondary Contact</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($team->specializations as $item)
                                            <tr>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->description }}</td>
                                                <td>{{ $item->primaryContact->username or '' }}</td>
                                                <td>{{ $item->secondaryContact->username or '' }}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="card ">
                        <div class="header">
                            <nav class="navbar navbar-default">
                                <div class="container-fluid">
                                    <div class="navbar-header">
                                        <div class="navbar-brand">Project Milestones</div>
                                    </div>
                                    <div class="collapse navbar-collapse">
                                            <ul class="nav navbar-nav navbar-right pull-right">
                                                <button class="btn btn-success btn-fill">Achieved</button>
                                                <button class="btn btn-danger btn-fill">Behind Schedule</button>
                                                <button class="btn btn-info btn-fill">On Track</button>
                                            </ul>
                                        </div>
                                    <div class="collapse navbar-collapse">

                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="content">
                            <div id="visualization"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                {{-- ChartJS Project Updates--}}
                <pi-line-graph chart-id="projectUpdatesChart" url="api/charts/dashboard/project-updates" chart-div-size="col-md-12"></pi-line-graph>
            </div>
            <div class="row">
                {{-- ChartJS Task Assignment --}}
                <pi-bar-graph chart-id="taskAssignmentChart" url="/api/charts/dashboard/task-assignment" chart-div-size="col-md-6"></pi-bar-graph>
                {{-- ChartJS Team Hours--}}
                <pi-bar-graph chart-id="staffHoursChart" url="/api/charts/dashboard/staff-hours" chart-div-size="col-md-6"></pi-bar-graph>
            </div>
            <div class="row">
                {{-- ChartJS Project Hours--}}
                <pi-pie-graph chart-id="projectHoursChart" url="/api/charts/dashboard/project-hours" chart-div-size="col-md-6 col-sm-12"></pi-pie-graph>
                {{-- ChartJS Team Hours--}}
                <pi-bar-graph chart-id="staffCompHoursChart" url="/api/charts/dashboard/comp-hours" chart-div-size="col-md-6 col-sm-12"></pi-bar-graph>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    @include('partials._timeline')

    @foreach($teams as $team)
        @include('partials._datatables', ['name' => $team->id . '_Table', 'sort' => 0, 'direction' => 'asc', 'pageLength' => 5])
    @endforeach

@endsection