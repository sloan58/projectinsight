
/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 *  App Packages
 */
require('./jquery.min.js');
require('./jquery-ui.min.js');
require('./bootstrap.min.js');
require('./jquery.validate.min.js');
require('./bootstrap-datetimepicker.js');
require('./bootstrap-selectpicker.js');
require('./bootstrap-checkbox-radio-switch-tags.js');
require('./chartist.min.js');
require('./bootstrap-notify.js');
require('./sweetalert2.js');
require('./jquery-jvectormap.js');
require('./jquery.bootstrap.wizard.min.js');
require('./bootstrap-table.js');
require('./light-bootstrap-dashboard.js');
require('./select2.min.js');
require('./jquery-ujs.js');
require('./laravel-bootstrap-modal-form.js');
require('./loginForm');

/**
 *  Node Packages
 */
require('datatables.net');
require('datatables.net-bs');
require('fullcalendar');
require('jquery-match-height');
require('chart.js');
require('qtip2');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate, {fieldsBagName: 'formFields'});

/**
 *  NPM Packages
 */
Vue.component('Datepicker', require('vuejs-datepicker'));
Vue.component('vuetable', require('vuetable-2/src/components/Vuetable'));
Vue.component('vuetable-pagination', require('vuetable-2/src/components/VuetablePagination'));
Vue.component('vuetable-pagination-info', require('vuetable-2/src/components/VuetablePaginationInfo'));
Vue.component('vue-events', require('vue-events'));

// Dropzone
Vue.component('my-dropzone', require('./components/MyDropzone.vue'));

// Main App Components
Vue.component('Annoucements', require('./components/Annoucements.vue'));

// Graph Components
Vue.component('PiBarGraph', require('./components/Graphs/BarGraph.vue'));
Vue.component('PiLineGraph', require('./components/Graphs/LineGraph.vue'));
Vue.component('PiPieGraph', require('./components/Graphs/PieGraph.vue'));

// Status Components
Vue.component('PiStatusTimeline', require('./components/Statuses/StatusTimeline.vue'));

// Projects Components
Vue.component('PiProjectsTable', require('./components/Projects/ProjectsTable.vue'));
Vue.component('PiProjectsTableActions', require('./components/Projects/ProjectsTableActions.vue'));
Vue.component('PiProjectsClosedStatus', require('./components/Projects/ProjectsClosedStatus.vue'));
Vue.component('PiProjectEdit', require('./components/Projects/ProjectEdit.vue'));
Vue.component('PiProjectTasksTable', require('./components/Projects/ProjectTasksTable.vue'));
Vue.component('PiProjectMilestonesTable', require('./components/Projects/ProjectMilestonesTable.vue'));
Vue.component('PiMilestonesTableActions', require('./components/Projects/MilestonesTableActions.vue'));
Vue.component('PiMilestonesAchievedStatus', require('./components/Projects/MilestonesAchievedStatus.vue'));

// Users Components
Vue.component('UsersTableActions', require('./components/Users/UsersTableActions.vue'));
Vue.component('UsersTable', require('./components/Users/UsersTable.vue'));

// Teams Components
Vue.component('TeamsTable', require('./components/Teams/TeamsTable.vue'));
Vue.component('TeamsTableActions', require('./components/Teams/TeamsTableActions.vue'));
Vue.component('TeamsManagersTable', require('./components/Teams/TeamsManagersTable.vue'));

// Expirations Components
Vue.component('ExpirationsTable', require('./components/Expirations/ExpirationsTable.vue'));
Vue.component('ExpirationsTableActions', require('./components/Expirations/ExpirationsTableActions.vue'));

// Tasks Components
Vue.component('TasksTable', require('./components/Tasks/TasksTable.vue'));
Vue.component('TasksTableActions', require('./components/Tasks/TasksTableActions.vue'));
Vue.component('TasksCompletedStatus', require('./components/Tasks/TasksCompletedStatus.vue'));

// Vuetable-2 Filter Bar
Vue.component('vue-table-filter-bar', require('./components/VueTableFilterBar.vue'));

const app = new Vue({
    el: "#vue-app",
});
