// import Vue from 'vue';

import VueResource from 'vue-resource';
import BarGraph from './components/Graphs/BarGraph';
import PieGraph from './components/Graphs/PieGraph';
import LineGraph from './components/Graphs/LineGraph';

Vue.use(VueResource);

new Vue({
    el: "#vue-graphs",
    data: {
      foo: 'bar'
    },
    components: { BarGraph, PieGraph, LineGraph }
});