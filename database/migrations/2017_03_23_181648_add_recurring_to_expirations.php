<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecurringToExpirations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expirations', function (Blueprint $table) {
            $table->boolean('recurring')->after('expiration_date')->default(0);
        });

        \DB::table('expirations')->update(['recurring' => 0]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expirations', function (Blueprint $table) {
            $table->dropColumn('recurring');
        });
    }
}
