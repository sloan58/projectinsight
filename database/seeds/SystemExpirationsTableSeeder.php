<?php

use Illuminate\Database\Seeder;

class SystemExpirationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Expiration::class, 50)->create();
    }
}
