<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Role::create([
            'name' => 'admin',
            'label' => 'Admin'
        ]);

        \App\Models\Role::create([
            'name' => 'manager',
            'label' => 'Manager'
        ]);

        \App\Models\Role::create([
            'name' => 'user',
            'label' => 'User'
        ]);
    }
}
