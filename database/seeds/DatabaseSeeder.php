<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Clean the tables
        $tables = [
            'roles',
            'users',
            'role_user',
            'teams',
            'projects',
            'tasks',
            'statuses',
            'comp_hours',
            'milestones',
            'expirations',
        ];

        // Remove foreign key checks
        \DB::statement("SET foreign_key_checks=0");
        foreach($tables as $table) {
            \DB::table($table)->truncate();
        }
        \DB::statement("SET foreign_key_checks=1");

        // Call Seeders
        $this->call(TeamsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(TeamLeadSeeder::class);
        $this->call(ProjectsTableSeeder::class);
        $this->call(TasksTableSeeder::class);
        $this->call(StatusUpdatesTableSeeder::class);
        $this->call(CompHoursTableSeeder::class);
        $this->call(MilestonesTableSeeder::class);
        $this->call(SystemExpirationsTableSeeder::class);
    }
}
