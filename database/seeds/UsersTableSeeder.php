<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = App\Models\User::create([
            'username' => 'Martin Sloan',
            'email' => 'martinsloan58@gmail.com',
            'password' => bcrypt('admin'),
            'team_id' => App\Models\Team::inRandomOrder()->pluck('id')->first(),
            'local_account' => true
        ]);
        $admin->roles()->sync(App\Models\Role::pluck('id')->toArray());
        $admin->managedTeams()->sync(App\Models\Team::pluck('id')->toArray());

        $admin = App\Models\User::create([
            'username' => 'Sam Park',
            'email' => 'upperlevelparking@gmail.com',
            'password' => bcrypt('admin'),
            'team_id' => App\Models\Team::inRandomOrder()->pluck('id')->first(),
            'local_account' => true
        ]);
        $admin->roles()->sync(App\Models\Role::pluck('id')->toArray());
        $admin->managedTeams()->sync(App\Models\Team::pluck('id')->toArray());

        $admin = App\Models\User::create([
            'username' => 'Demo Admin',
            'email' => 'demo@demo.com',
            'password' => bcrypt('admin123admin'),
            'team_id' => App\Models\Team::inRandomOrder()->pluck('id')->first(),
            'local_account' => true
        ]);
        $admin->roles()->sync(App\Models\Role::pluck('id')->toArray());
        $admin->managedTeams()->sync(App\Models\Team::pluck('id')->toArray());

        factory(App\Models\User::class, 100)->create();
    }
}
