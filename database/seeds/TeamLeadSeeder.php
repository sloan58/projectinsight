<?php

use Illuminate\Database\Seeder;

class TeamLeadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $teams = App\Models\Team::all();

        foreach($teams as $team) {
            $team->team_lead = $team->members()->inRandomOrder()->pluck('id')->first();
            $team->save();
        }
    }
}
