<?php

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class CompHoursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        $users = App\Models\User::all();

        foreach($users as $user) {
            $compEntries = $faker->numberBetween($min=0, $max=10);
            for($i = $compEntries; $i>0; $i--) {
                App\Models\CompHour::create([
                    'reason' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                    'hours' => $faker->numberBetween($min = 1, $max = 40),
                    'user_id' => $user->id,
                    'project_id' => $user->team->projects()->inRandomOrder()->pluck('id')->first()
                ]);
            }

            if($user->compHours->count()) {
                App\Models\CompHour::create([
                    'reason' => $faker->sentence($nbWords = 3, $variableNbWords = true),
                    'redeemed_hours' => $faker->numberBetween($min = 1, $max = $user->compHours->sum('hours')),
                    'user_id' => $user->id,
                ]);
            }
        }
    }
}
