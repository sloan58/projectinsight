<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Models\Team::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence($nbWords = 2, $variableNbWords = true),
    ];
});

$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'team_id' => App\Models\Team::inRandomOrder()->pluck('id')->first(),
        'local_account' => true,
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Models\Project::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'description' => $faker->sentence,
        'team_id' => App\Models\Team::inRandomOrder()->pluck('id')->first(),
        'is_closed' => $faker->boolean($chanceOfGettingTrue = 25)
    ];
});

$factory->define(App\Models\Task::class, function (Faker\Generator $faker) {
    $project = App\Models\Project::inRandomOrder()->first();
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'description' => $faker->sentence,
        'completed' => $faker->boolean($chanceOfGettingTrue = 25),
        'project_id' => $project->id,
        'assigned_to' => $project->team->members()->inRandomOrder()->pluck('id')->first(),
        'created_by' => $project->team->members()->inRandomOrder()->pluck('username')->first(),
    ];
});

$factory->define(App\Models\Status::class, function (Faker\Generator $faker) {
    $project = App\Models\Project::has('tasks')->inRandomOrder()->first();
    return [
        'title' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'body' => $faker->sentence,
        'user_id' => $project->team->members()->inRandomOrder()->pluck('id')->first(),
        'task_id' => $project->tasks()->inRandomOrder()->pluck('id')->first(),
        'hours_spent' => $faker->numberBetween($min = 1, $max = 12),
        'performed_at' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = 'now'),
        'completed' => $faker->boolean($chanceOfGettingTrue = 25),
        'admin_time' => $faker->boolean($chanceOfGettingTrue = 25)
    ];
});

$factory->define(App\Models\CompHour::class, function (Faker\Generator $faker) {
    return [
        'reason' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'hours' => $faker->numberBetween($min = 1, $max = 40),
        'user_id' => App\Models\User::inRandomOrder()->pluck('id')->first(),
    ];
});

$factory->define(App\Models\Milestone::class, function (Faker\Generator $faker) {
    $project = App\Models\Project::inRandomOrder()->first();
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'description' => $faker->sentence,
        'complete_by' => $faker->dateTimeBetween($startDate = '-1 months', $endDate = '+8 months'),
        'achieved' => $faker->boolean($chanceOfGettingTrue = 25),
        'project_id' => $project->id,
        'created_by' => $project->team->members()->inRandomOrder()->pluck('username')->first(),
        'assigned_to' => $project->team->members()->inRandomOrder()->pluck('id')->first(),
    ];
});

$factory->define(App\Models\Expiration::class, function (Faker\Generator $faker) {
    $team = App\Models\Team::inRandomOrder()->first();
    return [
        'name' => $faker->sentence($nbWords = 3, $variableNbWords = true),
        'description' => $faker->sentence,
        'vendor' => $faker->word,
        'price' => $faker->numberBetween($min = 1, $max = 1000000),
        'expiration_date' => $faker->dateTimeBetween($startDate = '-2 months', $endDate = '+2 years'),
        'created_by' => $team->members()->inRandomOrder()->pluck('username')->first(),
        'team_id' => $team->id,
    ];
});
