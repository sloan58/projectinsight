const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');
require('laravel-elixir-vueify');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application as well as publishing vendor resources.
 |
 */

elixir((mix) => {
     mix.copy('node_modules/font-awesome/fonts', 'public/build/fonts')
        .copy('resources/assets/fonts', 'public/build/fonts')
        .copy('resources/assets/img', 'public/img')
        .sass('app.scss').version('css/app.css')
        .webpack('app.js').version('js/app.js');
});
