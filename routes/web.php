<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'impersonate'], function()
{
    Route::get('/', function () {
        return view('auth.login');
    })->middleware('guest');

    Auth::routes();

    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/users/impersonate/{user}', 'UsersController@impersonate');
    Route::get('/users/stop', 'UsersController@stopImpersonate');
    Route::resource('users', 'UsersController');

    Route::post('projects/report', ['as' => 'projects.report', 'uses' => 'ProjectsController@report']);
    Route::get('projects/{project}/toggle-closed', 'ProjectsController@toggleClosed')->name('projects.toggle');
    Route::get('projects/{project}/milestones', 'ProjectsController@getMilestones')->name('projects.milestones');
    Route::resource('projects', 'ProjectsController', ['except' => ['create', 'show']]);

    Route::post('tasks/toggle-completed/{task}', 'TasksController@toggleCompleted')->name('tasks.toggle');
    Route::resource('tasks', 'TasksController', ['except' => ['create', 'show']]);

    Route::post('milestones/{milestone}/toggle-achieved', 'MilestonesController@toggleAchieved')->name('milestone.toggle');
    Route::resource('milestones', 'MilestonesController', ['only' => ['store', 'update', 'destroy']]);

    Route::resource('statuses', 'StatusesController');

    Route::post('expirations/{expiration}/remove-attachment', 'ExpirationsController@removeAttachment')->name('expirations.attachment.remove');
    Route::post('expirations/{expiration}/add-attachment', 'ExpirationsController@addAttachment')->name('expirations.attachment.add');
    Route::get('expiratioins/{expiration}/download-attachment/{attachment}', 'ExpirationsController@downloadAttachment')->name('expirations.attachment.download');
    Route::get('expirations/calendar', 'ExpirationsController@calendar')->name('expirations.calendar');
    Route::resource('expirations', 'ExpirationsController');

    Route::get('teams/{team}/managers', 'TeamsController@teamManagersTable');
    Route::resource('teams', 'TeamsController');

    Route::resource('teams.specializations', 'TeamsSpecializationsController');

    Route::resource('users', 'UsersController', ['only' => ['index', 'store', 'edit', 'update', 'destroy']]);

    Route::post('roles', 'RolesController@createRole');

    Route::resource('comp-hours', 'CompHoursController');

    Route::get('announcements', 'AnnouncementsController@index');
    Route::post('announcements', 'AnnouncementsController@store');
    Route::post('announcements/{announcement}/read', 'AnnouncementsController@read');
    Route::post('announcements/{announcement}/hide', 'AnnouncementsController@hide');

    Route::get('reporting/staff', function() {
        return view('reporting.staff-hours');
    })->name('reporting.staff');

    Route::get('dropzone', function() {
       return view('vuetable');
    });

    /*
     *  API Routes
     */
    Route::group(['prefix' => 'api'] , function () {

        // Milestones API
        Route::get('milestones/{user}', 'MilestonesController@timeline');

        // ChartJS API Routes
        Route::group(['prefix' => 'charts'], function () {

            // Dashboard Charts
            Route::group(['prefix' => 'dashboard'], function () {
                Route::get('project-updates', 'Charts\DashboardController@projectUpdatesChart');
                Route::get('task-assignment', 'Charts\DashboardController@taskAssignmentChart');
                Route::get('staff-hours', 'Charts\DashboardController@staffHoursChart');
                Route::get('project-hours', 'Charts\DashboardController@projectHoursChart');
                Route::get('comp-hours', 'Charts\DashboardController@staffCompHoursChart');
            });

            // User Profile Charts
            Route::group(['prefix' => 'user-profile'], function () {
                Route::get('hours/this-week/{userId}', 'Charts\UserProfileController@statusUpdateHoursThisWeekChart');
                Route::get('hours/all-weeks/{userId}', 'Charts\UserProfileController@statusUpdateHoursAllWeeksChart');
                Route::get('tasks/{userId}', 'Charts\UserProfileController@taskAssignmentStatsChart');
            });

            //Reporting Page Charts
            Route::group(['prefix' => 'reporting'], function() {
                Route::get('staff/wtd', 'Charts\ReportingController@staffWtd');
                Route::get('staff/last7', 'Charts\ReportingController@staffLast7');
            });
        });

    });
});
