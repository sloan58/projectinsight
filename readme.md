## About Project Insight

Project Insight is built on [Laravel 5.3](https://laravel.com) and provides a convenient interface for managing Projects, Tasks, Statuses and more!  A demo site is available [here](http://projectinsight.karmatek.io).
You can login as Demo Admin/admin123admin.

## Features
- Teams system to group users
- Create Projects and assign Tasks
- Post Statuses to your Tasks and track hours against the work
- Create Project Milestones
- Create System expiration events to track EOL/renewals and send notifications
- A Dashboard and User profile interface with charts and graphs to display your project data!
