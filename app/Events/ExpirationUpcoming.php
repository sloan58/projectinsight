<?php

namespace App\Events;

use App\Models\Expiration;
use Illuminate\Support\Facades\Log;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ExpirationUpcoming
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var Expiration
     */
    public $expiration;

    /**
     * Create a new event instance.
     *
     * @param Expiration $expiration
     */
    public function __construct(Expiration $expiration)
    {
        $this->expiration = $expiration;
        Log::debug('Expiration: Fired System Expiration Event', [$this->expiration]);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
