<?php

namespace App\Http\Controllers;

use App\Events\AnnouncementWasMade;
use App\Models\Announcement;
use Illuminate\Http\Request;

class AnnouncementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $announcements = \Auth::user()->announcements()->where('hide', 0)->with('createdBy')->get();
        return response()->json($announcements);
    }

    /**
     *  Store a new CompHour object
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['subject' => 'required']);
        $announcement = Announcement::create([
            'subject' => $request->get('subject'),
            'body' => $request->get('body'),
            'created_by' => \Auth::user()->id
        ]);

        event(new AnnouncementWasMade($announcement));

        return response()->json();
    }
    /**
     *  Mark an Announcement read for a User
     *
     * @param Announcement $announcement
     * @return \Illuminate\Http\JsonResponse
     */
    public function read(Announcement $announcement)
    {
        \Auth::user()->announcements()->updateExistingPivot($announcement->id, ['read' => 1]);
        return response()->json();
    }

    /**
     *  Hide an Announcement for a User
     *
     * @param Announcement $announcement
     * @return \Illuminate\Http\JsonResponse
     */
    public function hide(Announcement $announcement)
    {
        \Auth::user()->announcements()->updateExistingPivot($announcement->id, ['hide' => 1]);
        return response()->json();
    }
}
