<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Http\Request;

class TasksController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Tasks vuetable-2 AJAX Request
        if (request()->ajax()) {

            if(\Auth::user()->hasRole('manager')) {
                if(filter_var($request->input('showCompleted'), FILTER_VALIDATE_BOOLEAN)) {
                    $query = Task::with(['owner', 'project'])
                        ->whereHas('project', function($q) {
                            $q->whereIn('team_id', \Auth::user()->managedTeams->pluck('id')->toArray());
                        })
                        ->orderByVueTable();
                } else {
                    $query = Task::with(['owner', 'project'])
                        ->whereHas('project', function($q) {
                            $q->whereIn('team_id', \Auth::user()->managedTeams->pluck('id')->toArray());
                        })
                        ->where('completed', 0)
                        ->orderByVueTable();
                }

            } else {
                if(filter_var($request->input('showCompleted'), FILTER_VALIDATE_BOOLEAN)) {
                    $query = Task::with(['owner', 'project'])
                        ->whereIn('project_id', \Auth::user()->team->projects->pluck('id')->toArray())
                        ->orderByVueTable();
                } else {
                    $query = Task::with(['owner', 'project'])
                        ->whereIn('project_id', \Auth::user()->team->projects->pluck('id')->toArray())
                        ->where('completed', 0)
                        ->orderByVueTable();
                }
            }

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('name', 'like', $value)
                        ->orWhere('description', 'like', $value)
                        ->orWhereHas('owner', function($query) use ($value) {
                            $query->where('username', 'like', $value);
                        })
                        ->orWhereHas('project', function($query) use ($value) {
                            $query->where('name', 'like', $value);
                        })
                        ->orWhere('created_at', 'like', $value);
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }

        return view('tasks.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate the form request
        $this->validate($request, ['name' => 'required', 'assigned_to' => 'required', 'created_by' => 'required', 'project_id' => 'required']);

        try {
            $task = Task::create($request->all());
        } catch (\PDOException $e) {
            \Log::error('App\Models\Task PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        // Fire TaskWasCreated Event
//        Event::fire(new TaskWasCreated($task));

        $request->session()->flash('success', 'Task Added!');
        return response()->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        if(request()->ajax()) {
            return \DB::table('statuses')
                ->join('users', 'statuses.user_id', 'users.id')
                ->select(
                    'statuses.id',
                    'statuses.title',
                    'statuses.body',
                    'users.username',
                    'users.email',
                    'statuses.hours_spent',
                    'statuses.admin_time',
                    'statuses.performed_at',
                    'statuses.created_at'
                )
                ->where('statuses.task_id', $task->id)
                ->orderBy('statuses.created_at', 'desc')
                ->get();
        }
        return view('tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Task $task)
    {
        $this->validate($request, ['name' => 'required', 'assigned_to' => 'required']);

        $task->update($request->all());

        return redirect()->back()->with('success', 'Task Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function destroy(Task $task)
    {
        $task->delete();
        return response()->json();
    }

    /**
     * Toggle Task completed status
     *
     * @param Task $task
     * @return \Illuminate\Http\Response
     */
    public function toggleCompleted(Task $task)
    {
        $task->completed = !$task->completed;
        $task->save();

        return response()->json();
    }
}
