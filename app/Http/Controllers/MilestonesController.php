<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Milestone;
use Illuminate\Http\Request;

class MilestonesController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'complete_by' => 'required', 'project_id' => 'required', 'created_by' => 'required', 'assigned_to' => 'required']);

        try {
            Milestone::create($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\Milestone PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        $request->session()->flash('success', 'Milestone added!');
        return response()->json();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Milestone $milestone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Milestone $milestone)
    {
        $this->validate($request, ['name' => 'required', 'description' => 'required', 'complete_by' => 'required', 'assigned_to' => 'required']);
        $milestone->update($request->all());

        $request->session()->flash('success', 'Milestone updated!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Milestone $milestone
     * @return \Illuminate\Http\Response
     */
    public function destroy(Milestone $milestone)
    {
        $milestone->delete();
        return redirect()->back()->with('success', 'Milestone Deleted!');
    }

    /**
     *  Set the Milestone Achieved value
     *
     * @param $milestone
     * @return \Illuminate\Http\RedirectResponse
     */
    public function toggleAchieved($milestone)
    {
        $milestone->achieved = !$milestone->achieved;
        $milestone->save();

        return redirect()->back()
            ->with('success', 'Milestone Updated!')
            ->with('tab', 'milestones');
    }

    /**
     * @param User $user
     * @return string
     */
    public function timeline(User $user)
    {
        $timeline = [];

        // Get the team(s) to report on
        if ($user->hasRole('manager')) {
            $milestones = $user->projectMilestones();
        } else {
            $milestones = Milestone::whereIn('project_id', $user->team->projects()->pluck('id')->toArray())->get();
        }
        foreach($milestones as $key => $milestone) {

            $timeline[$key]['id'] = $milestone->id;
            $assignedTo = isset($milestone->assignedTo) ? $milestone->assignedTo->username : 'Unassigned';
            $timeline[$key]['content'] = "$milestone->name ({$milestone->project->name})<br/><b>Assigned To</b>: $assignedTo";
            $timeline[$key]['title'] = $milestone->complete_by->toFormattedDateString();
            $timeline[$key]['start'] = $milestone->complete_by->toDateString();
            // Milestone is past due
            if(! $milestone->achieved && $milestone->complete_by->lt(Carbon::now())) {
                $timeline[$key]['className'] = 'alert-danger';
                //Milestone achieved
            } elseif ($milestone->achieved && $milestone->complete_by->lt(Carbon::now())) {
                $timeline[$key]['className'] = 'alert-success';
                // Milestone in progress
            } else {
                $timeline[$key]['className'] = 'alert-info';
            }
        }

        return json_encode($timeline);
    }
}
