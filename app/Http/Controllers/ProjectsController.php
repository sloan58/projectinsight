<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Team;
use App\Models\Project;
use Illuminate\Http\Request;
use App\Jobs\GenerateProjectReportJob;
use Yajra\Datatables\Facades\Datatables;

class ProjectsController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Projects vuetable-2 AJAX Request
        if (request()->ajax()) {

            if(\Auth::user()->hasRole('manager')) {
                if(filter_var($request->input('showClosed'), FILTER_VALIDATE_BOOLEAN)) {
                    // Show Closed Projects for Manager
                    $query = Project::whereIn('team_id', \Auth::user()
                                ->managedTeams
                                ->pluck('id')
                                ->toArray())
                                ->with('team')
                                ->orderByVueTable();
                } else {
                    $query = Project::whereIn('team_id', \Auth::user()
                                ->managedTeams
                                ->pluck('id')
                                ->toArray())
                                ->with('team')
                                ->where('is_closed', 0)
                                ->orderByVueTable();
                }
            } else {
                if(filter_var($request->input('showClosed'), FILTER_VALIDATE_BOOLEAN)) {
                    // Show Projects for Non-Manager
                    $query = \Auth::user()
                                ->team
                                ->projects()
                                ->with('team')
                                ->orderByVueTable();;
                } else {
                    $query = \Auth::user()
                                ->team
                                ->projects()
                                ->with('team')
                                ->where('is_closed', 0)
                                ->orderByVueTable();
                }
            }

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('name', 'like', $value)
                        ->orWhere('description', 'like', $value)
                        ->orWhereHas('team', function($query) use ($value) {
                            $query->where('name', 'like', $value);
                        })
                        ->orWhere('created_at', 'like', $value);
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }

            $teams = Team::pluck('name', 'id');
            return view('projects.index', compact('teams'));
        }

        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', 'team_id' => 'required']);

        try {
            $project = Project::create($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\Project PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }
        // Fire ProjectWasCreated Event
//        Event::fire(new ProjectWasCreated($project, Auth::user()));

        $request->session()->flash('success', 'Project Added!');
        return response()->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Project $project)
    {
        // Projects->tasks vuetable-2 AJAX Request
        if (request()->ajax()) {

            if(filter_var($request->input('showCompleted'), FILTER_VALIDATE_BOOLEAN)) {
                $query = $project->tasks()->with('owner')->orderByVueTable();;
            } else {
                $query = $project->tasks()->with('owner')->where('completed', 0)->orderByVueTable();;
            }

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('name', 'like', $value)
                        ->orWhere('description', 'like', $value)
                        ->orWhereHas('owner', function($query) use ($value) {
                            $query->where('username', 'like', $value);
                        })
                        ->orWhere('created_at', 'like', $value);
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }

        $teams = Team::pluck('name', 'id');
        return view('projects.edit', compact('project', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Project $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $this->validate($request, ['name' => 'required', 'team_id' => 'required']);

        $project->update($request->all());

        $request->session()->flash('success', 'Project Updated!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Project $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();
        return redirect()->route('projects.index')->with('success', 'Project Deleted!');
    }

    /**
     * Toggle Project closed status
     *
     * @param Project $project
     * @return \Illuminate\Http\Response
     */
    public function toggleClosed(Project $project)
    {
        $project->is_closed = !$project->is_closed;

        $project->save();

        return redirect()->route('projects.edit', ['project' => $project->id])->with('info', 'Project Updated!');
    }

    /**
     * Generate a new Project report
     * Email the report to the authenticated User
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function report(Request $request)
    {
        // Get all request input
        $input = $request->all();

        // See if the report should span all dates
        if(isset($input['all_dates'])) {
            // Set $get_all_dates to true
            $start_date = null;
            $end_date = null;
            $get_all_dates = true;
        } else {
            // Get only projects between time periods
            $start_date_array = explode('-', $input['start_date']);
            $end_date_array = explode('-', $input['end_date']);

            $start_date = Carbon::create($start_date_array[0], $start_date_array[1], $start_date_array[2], 0, 0, 0, 'America/New_York')->toDateTimeString();
            $end_date = Carbon::create($end_date_array[0], $end_date_array[1], $end_date_array[2], 0, 0, 0, 'America/New_York')->addDay()->toDateTimeString();
            $get_all_dates = false;
        }

        $projects = Project::whereIn('team_id', \Auth::user()->managedTeams->pluck('id')->toArray())->where('is_closed', 0)->get();

        $this->dispatch(new GenerateProjectReportJob(\Auth::user(), $projects, $start_date, $end_date, $get_all_dates));

        return redirect()->back()->with('success', 'Project report generated.  Check your email!');
    }

    /**
     *  Display a listing of the Project Milestones
     *
     * @param Request $request
     * @param Project $project
     * @return \Illuminate\Http\JsonResponse
     */
    public function getMilestones(Request $request, Project $project)
    {
        // Project->milestones vuetable-2 AJAX Request
        if(filter_var($request->input('showAchieved'), FILTER_VALIDATE_BOOLEAN)) {
            $query = $project->milestones()->with('assignedTo')->orderByVueTable();;
        } else {
            $query = $project->milestones()->with('assignedTo')->where('achieved', 0)->orderByVueTable();;
        }

        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->where('name', 'like', $value)
                    ->orWhere('description', 'like', $value)
                    ->orWhere('created_by', 'like', $value)
                    ->orWhereHas('assignedTo', function($query) use ($value) {
                        $query->where('username', 'like', $value);
                    });
            });
        }
        return response()->json(
            $query->paginate(10)
        );
    }
}
