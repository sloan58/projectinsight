<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Status;
use Illuminate\Http\Request;

class StatusesController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'hours_spent' => 'required', 'user_id' => 'required', 'task_id' => 'required', 'admin_time' => 'required']);

        $input = $request->all();
        $input['performed_at'] = new Carbon($input['performed_at']);
        \Log::debug('post', [$input]);

        try {
            Status::create($input);
        } catch(\PDOException $e) {
            \Log::error('App\Models\Status PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        $request->session()->flash('success', 'Status Update added!');
        return response()->json();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Status $status
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Status $status)
    {
        $this->validate($request, ['title' => 'required', 'hours_spent' => 'required', 'admin_time' => 'required']);

        $input = $request->all();
        $input['performed_at'] = new Carbon($input['performed_at']);

        $status->update($input);

        $request->session()->flash('success', 'Status Updated!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Status $status
     * @return \Illuminate\Http\Response
     */
    public function destroy(Status $status)
    {
        $status->delete();
        return response()->json();
    }
}
