<?php

namespace App\Http\Controllers;

use App\Models\Specialization;
use App\Models\Team;
use Illuminate\Http\Request;

class TeamsSpecializationsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Team $team)
    {
        $this->validate($request, ['name' => 'required']);

        try {
            $specialization = new Specialization($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\Specialization PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }
        $team->specializations()->save($specialization);

        return redirect()->back()->with('success', 'Specialization Added!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Team $team
     * @param Specialization $specialization
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(Request $request, Team $team, Specialization $specialization)
    {
        $this->validate($request, ['name' => 'required']);

        $input = $request->all();

        if(empty($input['primary_contact'])){
            unset($input['primary_contact']);
        }
        if(empty($input['secondary_contact'])){
            unset($input['secondary_contact']);
        }

        $specialization->update($input);

        $request->session()->flash('success', 'Specialization Updated!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $team
     * @param Specialization $specialization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team, Specialization $specialization)
    {
        $specialization->delete();
        return redirect()->back()->with('success', 'Specialization Deleted!');
    }
}
