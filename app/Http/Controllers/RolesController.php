<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateRoleRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;

class RolesController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function showDash()
    {
        return view('users.manage_roles');
    }

    /**
     * Fetching all roles and all permissions for role-edit page
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchRoles(Request $request)
    {
        return response()->json([
            'roles' => Role::with('permissions')->get(),
            'permissions' => Permission::all()
        ]);

    }

    /**
     * Creating new role
     * @param CreateRoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createRole(CreateRoleRequest $request)
    {
        $role = Role::create([
            'label' => $request->get('label'),
            'name' => $request->get('name')
        ]);

        $role->save();
        
        return response()->json();
    }

    /**
     * Deleting role and managing role permissions
     * @param Request $request
     * @param Role $role
     */
    public function manageRole(Request $request, Role $role)
    {
        if ($request->isMethod('delete')) {
            $role->delete();
        }

        if ($request->isMethod('patch')) {
            if ($request->has('permission')) {
                $permission = Permission::find($request->get('permission')['id']);
                if ($role->hasPermission($permission->name)) {
                    $role->revokePermissionTo($permission);
                } else {
                    $role->givePermissionTo($permission);
                }
            }
        }
    }

    /**
     * Creating new permission
     * @param CreateRoleRequest $request is used because permission
     * has same fields and field restrictions as role
     */
    public function createPermission(CreateRoleRequest $request)
    {
        $role = Permission::create([
            'label' => $request->get('label'),
            'name' => $request->get('name')
        ]);

        $role->save();
    }

    /**
     * Deleting specified permission
     * @param Permission $permission
     */
    public function deletePermission(Permission $permission)
    {
        $permission->delete();
    }
}