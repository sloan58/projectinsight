<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Users vuetable-2 AJAX Request
        if (request()->ajax()) {

            $query = User::with('team')->orderByVueTable();

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('username', 'like', $value)
                        ->orWhere('email', 'like', $value)
                        ->orWhereHas('team', function($query) use ($value) {
                            $query->where('name', 'like', $value);
                        });
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }

        $roles = Role::pluck('label', 'id');
        $teams = Team::pluck('name', 'id');

        return view('users.index', compact('roles', 'teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8',
            'team_id' => 'required',
            'roles' => 'required|array'
        ]);

        try {
            $user = User::create($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\User PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }
        $user->password = bcrypt($request->get('password'));
        $user->local_account = true;
        $user->save();

        $user->roles()->sync($request->get('roles'));

        $request->session()->flash('success', 'User Added!');
        return response()->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('label', 'id');
        $teams = Team::pluck('name', 'id');
        if($user->team) {
            $projects = $user->team->projects->pluck('name', 'id');
        } else {
            $projects = [];
        }

        return view('users.edit', compact('user', 'roles', 'teams', 'projects'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Check to see if the password is non-blank
        if($request->password == '') {
            // Validate form input without password which was blank
            $this->validate($request, [
                'email' => 'required',
                'username' => 'required',
                'team_id' => 'required',
            ]);

            // Don't update the password field in the DB
            $input = $request->all();
            unset($input['password']);

        } else  {
            // Validate form input with password validation
            $this->validate($request, [
                'email' => 'required|unique:users,email,' . $user->id . '|email',
                'username' => 'required',
                'team_id' => 'required',
                'password' => 'required|min:8',
            ]);

            // Hash the new password
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
        }

        // Update the User model
        $user->update($input);

        // Sync the selected Roles
        if(!isset($input['roles'])) {
            $input['roles'] = [];
        }
        $user->roles()->sync($input['roles']);

        // Sync the Teams this User manages
        if(!isset($input['team_management'])) {
            $input['team_management'] = [];
        }
        $user->managedTeams()->sync($input['team_management']);

        return redirect()->back()->with('success', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json();
    }

    /**
     *  Impersonate a User
     *
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function impersonate(User $user)
    {
        // Guard against administrator impersonate
        if(Auth::user()->hasRole('admin'))
        {
            Auth::user()->setImpersonating($user->id);
            return redirect()->back()->with('success', 'You are now impersonating ' . $user->username . '!');

        } else {
            return redirect()->back()->with('danger', 'You are not authorized to impersonate other users!');
        }
    }

    /**
     *  Stop impersonating a User
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function stopImpersonate()
    {
        Auth::user()->stopImpersonating();

        return redirect()->back()->with('success', 'You are no longer impersonating, ' . Auth::user()->username . '!');
    }
}
