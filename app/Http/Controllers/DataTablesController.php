<?php

namespace App\Http\Controllers;

use App\Models\User;
use Yajra\Datatables\Facades\Datatables;

class DataTablesController extends Controller
{
    /**
     *  DataTables User Model API
     *
     * @return mixed
     */
    public function users()
    {
        return Datatables::of(User::with('team'))
            ->addColumn('actions', function($user) {
                return
                    '<td class="text-center">
                            <a href="users/' . $user->id . '/edit" class="btn btn-simple btn-warning btn-icon edit"><i class="fa fa-edit"></i></a>
                            <a href="users/' . $user->id . '"
                               rel="tooltip"
                               class="btn btn-simple btn-danger btn-icon remove"
                               data-method="delete"
                               data-remote="false"
                               data-confirm="Are you sure you want to delete ' . $user->name . '?">
                               <i class="fa fa-times"></i> 
                            </a>
                        </td>';
            })
            ->make(true);
    }
}
