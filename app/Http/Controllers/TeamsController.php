<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;

class TeamsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Teams vuetable-2 AJAX Request
        if (request()->ajax()) {

            $query = Team::query()->orderByVueTable();

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('name', 'like', $value);
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }
        return view('teams.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['name' => 'required', ]);

        try {
            Team::create($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\Team PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        $request->session()->flash('success', 'Team Added!');
        return response()->json();
    }

    /**
     * Show the Team Managers table.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function teamManagersTable(Request $request, Team $team)
    {
        // Teams vuetable-2 AJAX Request
        $query = $team->managers()->orderByVueTable();
        if ($request->exists('filter')) {
            $query->where(function ($q) use ($request) {
                $value = "%{$request->filter}%";
                $q->whereHas('managers', function($query) use($value) {
                    $query->where('username', 'like', $value);
                });
            });
        }
        return response()->json(
            $query->paginate(10)
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function edit(Team $team)
    {
        $specializations = $team->specializations;
        return view('teams.edit', compact('team', 'specializations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Team $team)
    {
        $this->validate($request, ['name' => 'required', ]);

        $team->update($request->all());

        return redirect()->back()->with('success', 'Team Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Team $team
     * @return \Illuminate\Http\Response
     */
    public function destroy(Team $team)
    {
        if($team->projects->count()) {
            return response()->json([
                'error' => [
                    'message' => 'Sorry, this Team still has Projects.<br>Please move them to another Team before deleting.'
                ]
            ], 500);
        }
        $team->delete();
        return response()->json();
    }
}
