<?php

namespace App\Http\Controllers;

use App\Models\CompHour;
use Illuminate\Http\Request;

class CompHoursController extends Controller
{
    /**
     *  Store a new CompHour object
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['reason' => 'required', 'user_id' => 'required']);
        $input = $request->input();

        try {
            CompHour::create($input);
        } catch (\PDOException $e) {
            \Log::error('App\Models\ComHour PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        $request->session()->flash('info', 'Comp Hours Redeemed!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param CompHour $compHour
     * @return \Illuminate\Http\Response
     */
    public function destroy(CompHour $compHour)
    {
        $compHour->delete();

        return redirect()->back()->with('info', 'Comp Hours Added Back!');
    }
}
