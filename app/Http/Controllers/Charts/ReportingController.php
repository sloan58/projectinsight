<?php

namespace App\Http\Controllers\Charts;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ChartBuilderService;

class ReportingController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function staffWtd(Request $request)
    {
        if(!$request->input('filter') || $request->input('filter') == 'All') {
            $filter = null;
        } else {
            $filter = $request->input('filter');
        }
        return response()->json(ChartBuilderService::reportingStaffWeekToDateChart($filter));
    }

    public function staffLast7(Request $request)
    {
        if(!$request->input('filter') || $request->input('filter') == 'All') {
            $filter = null;
        } else {
            $filter = $request->input('filter');
        }
        return response()->json(ChartBuilderService::reportingStaffLastSevenDaysChart($filter));
    }

}
