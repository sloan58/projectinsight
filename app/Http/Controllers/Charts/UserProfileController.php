<?php

namespace App\Http\Controllers\Charts;


use App\Models\User;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ChartBuilderService;

class UserProfileController extends Controller
{
    /**
     *  ChartJS showing Status Update hours_spent this week
     *
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusUpdateHoursThisWeekChart($userId)
    {
        $user = User::find($userId);
        return response()->json(ChartBuilderService::userProfileStatusUpdateHoursThisWeekChart($user));
    }

    /**
     *  ChartJS showing Status Update hours_spent all weeks
     *
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function statusUpdateHoursAllWeeksChart($userId)
    {
        $user = User::find($userId);
        return response()->json(ChartBuilderService::userProfileStatusUpdateHoursAllWeeksChart($user));
    }

    /**
     *  ChartJS showing Status Update hours_spent
     *
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function taskAssignmentStatsChart($userId)
    {
        $user = User::find($userId);
        return response()->json(ChartBuilderService::userProfileTaskAssignmentStatsChart($user));
    }
}
