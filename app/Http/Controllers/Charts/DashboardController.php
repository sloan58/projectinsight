<?php

namespace App\Http\Controllers\Charts;


use Illuminate\Http\Request;
use App\Services\ChartBuilderService;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     *  ChartJS showing Status Update activity
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectUpdatesChart()
    {
        return response()->json(ChartBuilderService::dashboardProjectUpdatesChart());
    }

    /**
     *  ChartJS showing tasks assigned by Team Members
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function taskAssignmentChart(Request $request)
    {
        if(!$request->input('filter') || $request->input('filter') == 'All') {
            $filter = null;
        } else {
            $filter = $request->input('filter');
        }
        return response()->json(ChartBuilderService::dashboardTaskAssignmentChart($filter));
    }

    /**
     *  ChartJS showing hours by Team Member
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function staffHoursChart(Request $request)
    {
        if(!$request->input('filter') || $request->input('filter') == 'All') {
            $filter = null;
        } else {
            $filter = $request->input('filter');
        }
        return response()->json(ChartBuilderService::dashboardStaffHoursChart($filter));
    }

    /**
     *  ChartJS showing project hours by Project
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function projectHoursChart()
    {
        return response()->json(ChartBuilderService::dashboardProjectHoursChart());
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function staffCompHoursChart(Request $request)
    {
        if(!$request->input('filter') || $request->input('filter') == 'All') {
            $filter = null;
        } else {
            $filter = $request->input('filter');
        }
        return response()->json(ChartBuilderService::dashboardStaffCompHoursChart($filter));
    }
}
