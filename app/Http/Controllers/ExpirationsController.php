<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Expiration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ExpirationsController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // Expirations vuetable-2 AJAX Request
        if (request()->ajax()) {

            $query = Expiration::query()->orderByVueTable();

            if ($request->exists('filter')) {
                $query->where(function ($q) use ($request) {
                    $value = "%{$request->filter}%";
                    $q->where('name', 'like', $value)
                        ->orWhere('description', 'like', $value)
                        ->orWhere('vendor', 'like', $value)
                        ->orWhere('reseller', 'like', $value)
                        ->orWhere('price', 'like', $value)
                        ->orWhere('created_by', 'like', $value)
                        ->orWhere('expiration_date', 'like', $value);
                });
            }
            return response()->json(
                $query->paginate(10)
            );
        }

        $teams = Team::pluck('name', 'id');
        return view('expirations.index', compact('teams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' =>'required',
            'vendor' => 'required',
            'reseller' => 'required',
            'price' => 'numeric',
            'expiration_date' => 'required',
            'team_id' => 'required',
            'created_by' => 'required'
        ]);

        \Log::info($request->all());

        try {
            Expiration::create($request->all());
        } catch(\PDOException $e) {
            \Log::error('App\Models\Expiration PDOException', [$e->getMessage()]);
            $request->session()->flash('danger', 'Sorry, something went wrong saving that information!');
            return response()->json();
        }

        $request->session()->flash('success', 'System Expiration added!');
        return response()->json();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Expiration $expiration
     * @return \Illuminate\Http\Response
     */
    public function edit(Expiration $expiration)
    {
        $teams = Team::pluck('name', 'id');

        return view('expirations.edit', compact('expiration', 'teams'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Expiration $expiration
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Expiration $expiration)
    {
        \Log::debug('exp update', [$request->all()]);
        $this->validate($request, [
            'name' => 'required',
            'vendor' => 'required',
            'reseller' => 'required',
            'price' => 'numeric',
            'expiration_date' => 'required',
            'team_id' => 'required'
        ]);

        $expiration->update($request->all());

        $request->session()->flash('success', 'System Expiration updated!');
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Expiration $expiration
     * @return \Illuminate\Http\Response
     */
    public function destroy(Expiration $expiration)
    {
        $expiration->delete();
        return response()->json();
    }

    /**
     *  Add an attachment to the Expiration
     *
     * @param Request $request
     * @param Expiration $expiration
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function addAttachment(Request $request, Expiration $expiration)
    {
        $fileName = str_replace(' ', '', $request->file('file')->getClientOriginalName());
        $attachments = (array) json_decode($expiration->attachments);

        if(in_array($fileName, $attachments)) {
            return redirect()->back()->with('danger', 'Sorry, that filename exists already.');
        }

        $path = $request->file('file')->storeAs(
            'expirations/attachments/' . $expiration->id, $fileName , 'public'
        );

        if($path) {
            array_push($attachments, $fileName);
            $expiration->attachments = json_encode($attachments);
            $expiration->save();
            return response()->json('success', 200);
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     *  WIP - Remove an attachment from the Expiration
     *
     * @param Request $request
     * @param Expiration $expiration
     */
    public function removeAttachment(Request $request, Expiration $expiration)
    {
        \Log::debug('request', [$request->all()]);
    }

    /**
     *  Download an attachment from the Expiration
     * 
     * @param Expiration $expiration
     * @param $attachment
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadAttachment(Expiration $expiration, $attachment)
    {
        return response()->download(storage_path('app/public/expirations/attachments/'. $expiration->id . '/' . $attachment));
    }

    /**
     *  Generate the Expirations Calendar using FullCalendar.js
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function calendar()
    {
        $expirations = Expiration::all();

        $events = [];
        foreach($expirations as $expiration) {
            $events[] = \Calendar::event(
                $expiration->name, //event title
                true, //full day event?
                $expiration->expiration_date->toDateString(), //start time (you can also use Carbon instead of DateTime)
                $expiration->expiration_date->toDateString(), //end time (you can also use Carbon instead of DateTime)
                0, //optionally, you can specify an event ID
                [
                    'description' => '<b>Description</b>: ' . $expiration->description . '<br>' .
                                     '<b>Vendor</b>: ' . $expiration->vendor . '<br>' .
                                     '<b>Reseller</b>: ' . $expiration->reseller
                ]
            );
        }

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 0,
            'header' => [
                'left' => 'month,agendaWeek,agendaDay',
                'center' => 'title',
                'right' => 'prev,next today'
            ]
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'eventRender' => 'function(event, element) {
                                element.qtip({
                                    content: event.description,
                                    style: {
                                        classes: "qtip-bootstrap"
                                        },
                                        position: {
                                            at: "top center"
                                        }
                                    });
                                }',
            'eventMouseover' => 'function(event) {
                                    jQuery(this).qtip();
                                }'
        ]);

        $teams = Team::pluck('name', 'id');

        return view('expirations.calendar', compact('teams', 'calendar'));
    }
}
