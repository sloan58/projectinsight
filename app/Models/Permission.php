<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    protected $table = 'permissions';

    protected $fillable = [ 
        'name', 
        'label' 
    ];

    /**
     * Relation to roles
     *
     */
    public function roles() 
    {
        return $this->belongsToMany(Role::class);
    }
}
