<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Riesjart\VueTable\Traits\VueTableSortable;

class Project extends Model
{
    use VueTableSortable;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'team_id'
    ];

    /**
     *  Relations to eager load automatically
     * @var array
     */
    protected $with = [
        'team'
    ];

    /**
     * A Project belongs to a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * A Project has many Tasks
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class);
    }

    /**
     *  A Project has many Milestones
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function milestones()
    {
        return $this->hasMany(Milestone::class);
    }
}
