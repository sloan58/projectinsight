<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'primary_contact', 'secondary_contact'
    ];

    /**
     * A Specialization belongs to a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     *  A Specialization has a Primary Contact (User)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function primaryContact()
    {
        return $this->belongsTo(User::class, 'primary_contact');
    }

    /**
     *  A Specialization has a Secondary Contact (User)
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function secondaryContact()
    {
        return $this->belongsTo(User::class, 'secondary_contact');
    }
}
