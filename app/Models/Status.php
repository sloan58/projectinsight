<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'body', 'user_id', 'task_id', 'hours_spent', 'performed_at', 'admin_time'
    ];

    /**
     *  Attributes as Carbon Objects
     *
     * @var array
     */
    protected $dates = [
        'performed_at'
    ];

    /**
     * A Status update belongs to a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * A Status Update belongs to a Task
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function task()
    {
        return $this->belongsTo(Task::class);
    }
}
