<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Riesjart\VueTable\Traits\VueTableSortable;

class Expiration extends Model
{
    use VueTableSortable;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description',
        'vendor',
        'reseller',
        'price',
        'expiration_date',
        'recurring',
        'attachments',
        'created_by',
        'team_id'
    ];

    /**
     *  Attributes as Carbon Objects
     */
    protected $dates = [
      'expiration_date'
    ];
    /**
     *  An Expiration belongs to a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }
}
