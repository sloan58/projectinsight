<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'label'
    ];

    /**
     * Relation
     *
     */
    public function users() 
    {
        return $this->hasMany(User::class);
    }

    /**
     * Role has many permissions
     *
     */
    public function permissions() 
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Adding a permission to the role
     *
     */
    public function givePermissionTo(Permission $permission) 
    {
        return $this->permissions()->save($permission);
    }

    /**
     * Revoking permission from role
     *
     */
    public function revokePermissionTo(Permission $permission) 
    {
        return $this->permissions()->detach($permission->id);
    }

    /**
     * Analog of the hasRole function in roleable trait
     *
     */
    public function hasPermission($permission) 
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }
    }
}
