<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Riesjart\VueTable\Traits\VueTableSortable;

class Task extends Model
{
    use VueTableSortable;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'completed', 'project_id', 'assigned_to', 'created_by'
    ];

    /**
     * A Task belongs to a Project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    /**
     * A Task has many Statuses
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(Status::class);
    }

    /**
     * A Task is owned by a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }
}
