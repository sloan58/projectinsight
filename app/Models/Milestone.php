<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Riesjart\VueTable\Traits\VueTableSortable;

class Milestone extends Model
{
    use VueTableSortable;

    /*
     *  The database fields treated as Carbon Objects
     */
    protected $dates = ['complete_by'];

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'complete_by', 'achieved', 'project_id', 'created_by', 'assigned_to'
    ];

    /**
     *  A User Belongs To a Milestone
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assignedTo()
    {
        return $this->belongsTo(User::class, 'assigned_to');
    }

    /**
     *  A Milestone Belongs To a Project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
