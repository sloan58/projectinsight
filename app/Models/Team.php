<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Riesjart\VueTable\Traits\VueTableSortable;

class Team extends Model
{
    use VueTableSortable;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'direct_manager',
        'team_lead'
    ];

    /**
     * A Team can have many Members
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function members()
    {
        return $this->hasMany(User::class);
    }

    /**
     *  A Team can have many managers
     *
     * @return mixed
     */
    public function managers()
    {
        return $this->belongsToMany(User::class, 'manager_team', 'team_id', 'manager_id');
    }

    /**
     * A Team can have many Projects
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects()
    {
        return $this->hasMany(Project::class);
    }

    /**
     *  A Team has many Specializations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|mixed
     */
    public function specializations()
    {
        return $this->hasMany(Specialization::class);
    }

    /**
     *  A Team has a Direct Manager
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function directManager()
    {
        return $this->belongsTo(User::class, 'direct_manager');
    }

    /**
     *  A Team has a Team Lead
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo(User::class, 'team_lead');
    }

}
