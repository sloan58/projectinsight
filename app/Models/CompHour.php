<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompHour extends Model
{
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reason', 'hours', 'redeemed_hours', 'project_id', 'user_id'
    ];

    /**
     *  A CompHour belongs to a User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  Comp Hours Belong To a Project
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function project()
    {
        return $this->belongsTo(Project::class);
    }
}
