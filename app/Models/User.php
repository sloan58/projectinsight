<?php

namespace App\Models;

use Carbon\Carbon;
use App\Traits\Roleable;
use Illuminate\Support\Facades\Session;
use Illuminate\Notifications\Notifiable;
use Riesjart\VueTable\Traits\VueTableSortable;
use Adldap\Laravel\Traits\AdldapUserModelTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable, Roleable, AdldapUserModelTrait, VueTableSortable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'email', 'password', 'team_id', 'local_account'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     *  A User belongs to many Roles
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     *  A User belongs to a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function team()
    {
        return $this->belongsTo(Team::class);
    }

    /**
     *  A User can manage many Teams
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function managedTeams()
    {
        return $this->belongsToMany(Team::class, 'manager_team', 'manager_id');
    }

    /**
     * A User has many assigned Tasks
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks()
    {
        return $this->hasMany(Task::class, 'assigned_to');
    }

    /**
     *  A User has many Status Updates
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(Status::class);
    }

    /**
     *  A User has many CompHours
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function compHours()
    {
        return $this->hasMany(CompHour::class);
    }

    /**
     *  Get a users available comp time
     *
     * @return int
     */
    public function availableCompHours()
    {
        return $this->compHours->sum('hours') - $this->compHours->sum('redeemed_hours');
    }

    /**
     *  Get User hours submitted by week total
     * @return mixed
     */
    public function totalWeeklyHours()
    {
        $weekly_hours = [];
        $first_update = $this->statuses()->orderBy('performed_at', 'asc')->first();
        if(!$first_update) {
            return false;
        }
        $start_date = $first_update->performed_at->startOfWeek();
        $end_date = Carbon::now()->addWeek()->endOfWeek();
        while ($start_date->endOfWeek()->lt($end_date)) {
            $start_date->startOfWeek();
            $end_date_loop = clone $start_date;
            $end_date_loop->endOfWeek();
            $hours = Status::where('user_id', $this->id)->whereBetween('performed_at', [$start_date, $end_date_loop])->sum('hours_spent');
            $weekly_hours[$start_date->toDateString()] = $hours;
            $start_date->addWeek();
        }
        return $weekly_hours;
    }

    /**
     *  Get User hours submitted this week
     *
     * @return mixed
     */
    public function hoursThisWeek()
    {
        return Status::where('user_id', $this->id)->whereBetween('performed_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()])->sum('hours_spent');
    }

    /**
     *  A User has many Milestones
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function milestones()
    {
        return $this->hasMany(Milestone::class, 'assigned_to');
    }

    /**
     *  Milestones where a Project is managed by this User
     *
     * @return mixed
     */
    public function projectMilestones()
    {
        if(!$this->hasRole('manager')) {
            return Milestone::with('project')->whereIn('project_id', $this->team->projects->pluck('id'))->get();
        }
        return Milestone::with('project')->whereIn('project_id', Project::whereIn('team_id', $this->managedTeams()->pluck('id')->toArray())->pluck('id')->toArray())->get();
    }

    /**
     *  A User has many Announcements
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function myAnnouncements()
    {
        return $this->hasMany(Announcement::class, 'created_by');
    }

    /**
     *  A User receives many announcements
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function announcements()
    {
        return $this->belongsToMany(Announcement::class)->withPivot('read', 'hide');
    }
    /**
     *  Set the Impersonation User ID
     *
     * @param $id
     */
    public function setImpersonating($id)
    {
        Session::put('impersonate', $id);
    }

    /**
     * Stop impersonating the User
     */
    public function stopImpersonating()
    {
        Session::forget('impersonate');
    }

    /*
     *  Check if we are impersonating a User
     */
    public function isImpersonating()
    {
        return Session::has('impersonate');
    }

    /**
     *  A User has many Primary Specializations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function primarySpecializations()
    {
        return $this->hasMany(Specialization::class, 'primary_contact');
    }

    /**
     *  A User has many Secondary Specializations
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function secondarySpecializations()
    {
        return $this->hasMany(Specialization::class, 'secondary_contact');
    }

    /**
     *  A User Directly Manages a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function directlyManagesTeam()
    {
        return $this->hasOne(Team::class, 'direct_manager');
    }

    /**
     *  A User leads a Team
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function leadsTeam()
    {
        return $this->hasOne(Team::class, 'team_lead');
    }
}
