<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Task;
use App\Models\User;

class ChartBuilderService
{

    /**
     *  Build up to ChartJS Bar Chart array data
     *
     * @param $title
     * @param $chart_name
     * @param $category
     * @param $labels
     * @param $metric
     * @param $data
     * @param $background_color
     * @param $hover_colors
     * @param $stats
     * @param string $filters
     * @param bool $isManager
     * @param bool $selectedFilter
     * @return array
     */
    public static function generateChartJsBarChart($title, $chart_name, $category, $labels, $metric, $data, $background_color, $hover_colors, $stats, $filters = null, $isManager = false, $selectedFilter = false)
    {
        // Create the ChartJS vars
        return [
            'title' => $title,
            'chart_name' => $chart_name,
            'category' => $category,
            'labels' => $labels,
            'metric' => $metric,
            'data' => $data,
            'background_colors' => $background_color,
            'hover_colors' => $hover_colors,
            'stats' => $stats,
            'filters' => $filters,
            'isManager' => $isManager,
            'selectedFilter' => $selectedFilter
        ];
    }

    /**
     *  Build up to ChartJS Pie Chart array data
     *
     * @param $title
     * @param $chart_name
     * @param $category
     * @param $labels
     * @param $metric
     * @param $data
     * @param $background_color
     * @param $stats
     * @return array
     */
    public static function generateChartJsPieChart($title, $chart_name, $category, $labels, $metric, $data, $background_color, $stats)
    {
        // Create the ChartJS vars
        return [
            'title' => $title,
            'chart_name' => $chart_name,
            'category' => $category,
            'labels' => $labels,
            'metric' => $metric,
            'data' => $data,
            'background_colors' => $background_color,
            'hover_colors' => $background_color,
            'stats' => $stats
        ];
    }


    /**
     *  Produce graph data for team task assignment
     *
     * @param $filter
     * @return array
     */
    public static function dashboardTaskAssignmentChart($filter)
    {
        // Get the team(s) to report on
        $teams = self::getTeamsForReporting($filter);

        // Get the assigned tasks for either the users team or the managers teams
        $engineering_task_assignments = collect(\DB::table('users')
            ->select(\DB::raw('users.username user, count(tasks.id) tasks'))
            ->join('tasks', 'tasks.assigned_to', '=', 'users.id')
            ->join('teams', 'teams.id', '=', 'users.team_id')
            ->whereIn('users.team_id', $teams)
            ->where('tasks.completed', 0)
            ->groupBy('users.username')
            ->orderBy('tasks', 'desc')
            ->pluck('tasks', 'user'));

        // Get the last task assigned to this team
        $last_task_assigned = Task::orderBy('created_at', 'desc')->whereIn('assigned_to', \DB::table('users')->select('id')->whereIn('team_id', $teams))->first();

        // Generate Chart Labels
        $labels = self::generateChartLabels($engineering_task_assignments);

        // Set the stats label
        $stats = $last_task_assigned ? "Last Task assigned <b>{$last_task_assigned->created_at->diffForHumans()}</b> to <b>{$last_task_assigned->owner->username}</b> ($last_task_assigned->name)" : "No Data";

        // Get the team names for filtering
        $teamNames = self::getTeamsForFilter();

        // Only show filter if the user is a manager
        $isManager = \Auth::user()->hasRole('manager') ? true : false;

        // Return the ChartJS Bar Chart
        return self::generateChartJsBarChart(
            'Task Assignment - ' . (\Auth::user()->hasRole('manager') ?  ' Management View' : \Auth::user()->team->name),
            'TaskAssignment',
            'Tasks Assigned by Count',
            $labels,
            'Count',
            $engineering_task_assignments->values(),
            \Colors\RandomColor::many($engineering_task_assignments->keys()->count()),
            array_flatten(array_fill(1, $engineering_task_assignments->keys()->count(), '#00C0EF')),
            $stats,
            $teamNames,
            $isManager,
            $filter
        );
    }

    /**
     *  Produce graph data for team hours spent on tasks
     *
     * @param $filter
     * @return array
     */
    public static function dashboardStaffHoursChart($filter)
    {
        // Get the team(s) to report on
        $teams = self::getTeamsForReporting($filter);

        // Get the assigned tasks for either the users team or the managers teams
        $engineering_staff_hours = collect(\DB::table('statuses')
            ->select(\DB::raw('users.username user, SUM(statuses.hours_spent) hours'))
            ->join('users', 'users.id', '=', 'statuses.user_id')
            ->whereIn('users.team_id', $teams)
            ->whereBetween('statuses.performed_at', [Carbon::now()->startOfWeek(), Carbon::now()])
            ->groupBy('users.username')
            ->orderBy('hours', 'desc')
            ->pluck('hours', 'user'));


        // Generate Chart Labels
        $labels = self::generateChartLabels($engineering_staff_hours);

        // Set the stats label
        $stats = count($engineering_staff_hours->keys()) ? "Most hours spent is <b>" . key(reset($engineering_staff_hours)) . "</b> with " . current(reset($engineering_staff_hours)) . " hours" : "No Data";

        // Get the team names for filtering
        $teamNames = self::getTeamsForFilter();

        // Only show filter if the user is a manager
        $isManager = \Auth::user()->hasRole('manager') ? true : false;

        // Return the ChartJS Bar Chart
        return self::generateChartJsBarChart(
            'Staff Hours - ' . (\Auth::user()->hasRole('manager') ?  ' Management View' : \Auth::user()->team->name),
            'StaffHours',
            'Hours spent on tasks this week',
            $labels,
            'Hours',
            $engineering_staff_hours->values(),
            \Colors\RandomColor::many($engineering_staff_hours->keys()->count()),
            array_flatten(array_fill(1, $engineering_staff_hours->keys()->count(), '#00C0EF')),
            $stats,
            $teamNames,
            $isManager,
            $filter
        );
    }

    /**
     * Produce graph data for Project Time
     *
     * @return array
     */
    public static function dashboardProjectHoursChart() {

        // Get the team(s) to report on
        $teams = self::getTeamsForReporting();

        $projects_and_time_spent = collect(\DB::table('projects')
            ->select(\DB::raw('projects.name project, SUM(statuses.hours_spent) hours'))
            ->join('tasks', 'tasks.project_id', '=', 'projects.id')
            ->join('statuses', 'statuses.task_id', '=', 'tasks.id')
            ->join('users', 'users.id', '=', 'tasks.assigned_to')
            ->join('teams', 'teams.id', '=', 'users.team_id')
            ->whereIn('users.team_id', $teams)
            ->whereDate('statuses.performed_at', '>=', Carbon::today()->subMonth())
            ->groupBy('project')
            ->orderBy('hours', 'desc')
            ->take(10)
            ->pluck('hours', 'project'));

        $stats = count($projects_and_time_spent->keys()) ? "Most hours spent is <b>" . key(reset($projects_and_time_spent)) . "</b> with " . current(reset($projects_and_time_spent)) . " hours" : "No Data";

        // Return the ChartJS Pie Chart
        return self::generateChartJsPieChart(
            'Project Hours - ' . (\Auth::user()->hasRole('manager') ?  ' Management View' : \Auth::user()->team->name) . ' (Past 1 Month)',
            'ProjectHours',
            'Time contributions by Project (Top 10)',
            $projects_and_time_spent->keys(),
            'Hours',
            $projects_and_time_spent->values(),
            \Colors\RandomColor::many($projects_and_time_spent->keys()->count()),
            $stats
        );

    }

    /**
     *  Produce graph data for Status Update activity
     *
     * @return array
     */
    public static function dashboardProjectUpdatesChart() {

        // Get the team(s) to report on
        $teams = self::getTeamsForReporting();

        $project_updates_past_week = [];

        for($i=7; $i>=0; $i--) {
            $res = \DB::table('statuses')
                ->select(\DB::raw('count(statuses.id) count'))
                ->join('tasks', 'tasks.id', '=', 'statuses.task_id')
                ->join('users', 'users.id', '=', 'tasks.assigned_to')
                ->join('teams', 'teams.id', '=', 'users.team_id')
                ->whereIn('users.team_id', $teams)
                ->whereBetween('statuses.performed_at', [
                    Carbon::now()->subDays($i)->startOfDay(),
                    Carbon::now()->subDays($i)->endOfDay()
                ])->pluck('count');
            $project_updates_past_week[Carbon::now()->subDays($i)->format('m-d-Y')] = $res[0];
        }

        $labels = array_keys($project_updates_past_week);
        $values = array_values($project_updates_past_week);

        // Return the ChartJS Line Chart
        return self::generateChartJsBarChart(
            'Project Updates - ' . (\Auth::user()->hasRole('manager') ?  ' Management View' : \Auth::user()->team->name),
            'ProjectUpdates',
            'Updates submitted in the past week',
            $labels,
            'Updates',
            $values,
            \Colors\RandomColor::many(count($project_updates_past_week)),
            array_flatten(array_fill(1, count($project_updates_past_week), '#00C0EF')),
            'Status Update posts for the past week'
        );
    }

    /**
     *  Produce graph data for Comp Hours
     * @param $filter
     * @return array
     */
        public static function dashboardStaffCompHoursChart($filter)
    {
        // Get the team(s) to report on
        $teams = self::getTeamsForReporting($filter);

        $users = \DB::table('users')->select('users.id')->join('teams', 'teams.id', '=', 'users.team_id')->whereIn('users.team_id', $teams)->get();

        $user_comp_hours = [];

        foreach($users as $user) {
            $user = User::find($user->id);
            $user_comp_hours[$user->username] = $user->availableCompHours();
        }

        $labels = array_keys($user_comp_hours);
        $values = array_values($user_comp_hours);

        // Get the team names for filtering
        $teamNames = self::getTeamsForFilter();

        // Only show filter if the user is a manager
        $isManager = \Auth::user()->hasRole('manager') ? true : false;

        // Return the ChartJS Pie Chart
        return self::generateChartJsBarChart(
            'Staff Comp Hours - ' . (\Auth::user()->hasRole('manager') ?  ' Management View' : \Auth::user()->team->username),
            'StaffCompHours',
            'Comp Hours',
            $labels,
            'Comp Hours',
            $values,
            \Colors\RandomColor::many(count($user_comp_hours)),
            array_flatten(array_fill(1, count($user_comp_hours), '#00C0EF')),
            'Comp Hours',
            $teamNames,
            $isManager,
            $filter
        );

    }

    /**
     *  Produce graph data for Status Update hours_spent
     *
     * @param User $user
     * @return array
     */
    public static function userProfileStatusUpdateHoursThisWeekChart(User $user) {

        $status_update_hours_past_week = [];

        for($i=7; $i>=0; $i--) {
            $res = $user->statuses()->whereDate('performed_at', '=', Carbon::now()->subDays($i)->format('Y-m-d'))->sum('hours_spent');
            $status_update_hours_past_week[Carbon::now()->subDays($i)->format('Y-m-d')] = $res;
        }

        $labels = array_keys($status_update_hours_past_week);
        $values = array_values($status_update_hours_past_week);

        // Return the ChartJS Line Chart
        return self::generateChartJsBarChart(
            'Status Update Hours - This Week',
            'HoursThisWeek',
            'Hours in the past week',
            $labels,
            'Hours',
            $values,
            \Colors\RandomColor::many(count($status_update_hours_past_week)),
            array_flatten(array_fill(1, count($status_update_hours_past_week), '#00C0EF')),
            'Status Update hours for the past week'
        );
    }

    /**
     * @param User $user
     * @return array
     */
    public static function userProfileStatusUpdateHoursAllWeeksChart(User $user) {

        $weekly_hours = $user->totalWeeklyHours();

        $labels = array_keys($weekly_hours);
        $values = array_values($weekly_hours);

        // Return the ChartJS Line Chart
        return self::generateChartJsBarChart(
            'Status Update Hours - All Weeks',
            'HoursAllWeeks',
            'Hours for all weeks',
            $labels,
            'Hours',
            $values,
            \Colors\RandomColor::many(count($weekly_hours)),
            array_flatten(array_fill(1, count($weekly_hours), '#00C0EF')),
            'Status Update hours for all weeks'
        );

    }

    /**
     *  Produce graph data for Tasks open/closed
     * @param User $user
     * @return array
     */
    public static function userProfileTaskAssignmentStatsChart(User $user)
    {
        $open_tasks = $user->tasks->where('completed', 0)->count();
        $closed_tasks = $user->tasks->where('completed', 1)->count();

        // Return the ChartJS Pie Chart
        return self::generateChartJsPieChart(
            'Task Assignment Stats - ' . $user->username,
            'Tasks',
            'Tasks Open/Closed',
            ['Open', 'Closed'],
            'Tasks',
            [$open_tasks, $closed_tasks],
            \Colors\RandomColor::many(2),
            'Task statistics - Open and Closed'
        );
    }

    public static function reportingStaffWeekToDateChart($filter)
    {
        $user_hours_wtd = [];

        $teams_i_manage = \Auth::user()->managedTeams()->with('members')->get();

        foreach($teams_i_manage as $team) {
            // Get task, admin and total time spent since the start of the week
            foreach($team->members as $user) {
                // Admin Time
                $user_hours_wtd[$user->username][] = round($user->statuses()->where('admin_time', 1)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()])->sum('hours_spent'));
                // Task Time
                $user_hours_wtd[$user->username][] = round($user->statuses()->where('admin_time', 0)->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()])->sum('hours_spent'));
                // Total Time
                $user_hours_wtd[$user->username][] = round($user->statuses()->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()])->sum('hours_spent'));
            }
        }

        // Set labels and values
        $labels = array_keys($user_hours_wtd);
        $values = array_values($user_hours_wtd);

        // Get the team names for filtering
        $teamNames = self::getTeamsForFilter();

        // Only show filter if the user is a manager
        $isManager = \Auth::user()->hasRole('manager') ? true : false;

        // Return the ChartJS Line Chart
        return self::generateChartJsBarChart(
            'Staff Hours - Week To Date',
            'StaffWtd',
            'Hours for this week',
            $labels,
            ['Admin Hours', 'Task Hours', 'Total Hours'],
            $values,
            ['#f1c40f', '#3498db', '#e74c3c'],
            ['#f1c40f', '#3498db', '#e74c3c'],
            'Staff Hours - Week To Date',
            $teamNames,
            $isManager,
            $filter
        );
    }

    public static function reportingStaffLastSevenDaysChart($filter)
    {
        $user_hours_past_7 = [];

        $teams_i_manage = \Auth::user()->managedTeams()->with('members')->get();

        foreach($teams_i_manage as $team) {
            // Get task, admin and total time spent in the past 7 days
            foreach($team->members as $user) {
                // Admin Time
                $user_hours_past_7[$user->username][] = round($user->statuses()->where('admin_time', 1)->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])->sum('hours_spent'));
                // Task Time
                $user_hours_past_7[$user->username][] = round($user->statuses()->where('admin_time', 0)->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])->sum('hours_spent'));
                // Total Time
                $user_hours_past_7[$user->username][] = round($user->statuses()->whereBetween('created_at', [Carbon::now()->subWeek(), Carbon::now()])->sum('hours_spent'));
            }
        }

        // Set labels and values
        $labels = array_keys($user_hours_past_7);
        $values = array_values($user_hours_past_7);

        // Get the team names for filtering
        $teamNames = self::getTeamsForFilter();

        // Only show filter if the user is a manager
        $isManager = \Auth::user()->hasRole('manager') ? true : false;

        // Return the ChartJS Line Chart
        return self::generateChartJsBarChart(
            'Staff Hours - Last 7 Days',
            'StaffWtd',
            'Hours for the last 7 Days',
            $labels,
            ['Admin Hours', 'Task Hours', 'Total Hours'],
            $values,
            ['#2ecc71', '#e67e22', '#2c3e50'],
            ['#2ecc71', '#e67e22', '#2c3e50'],
            'Staff Hours - Last 7 Days',
            $teamNames,
            $isManager,
            $filter
        );
    }

    /**
     * @param null $filter
     * @return Illuminate\Http\Request|Illuminate\Support\Collection|array|string
     */
    public static function getTeamsForReporting($filter = null)
    {
        // Get the team(s) to report on
        if (\Auth::user()->hasRole('manager')) {
            if($filter) {
                $teams = \Auth::user()->managedTeams->where('name', $filter)->pluck('id');
            } else {
                $teams = \Auth::user()->managedTeams->pluck('id');
            }
            return $teams;
        } else {
            $teams = (array) \Auth::user()->team->id;
            return $teams;
        }
    }

    /**
     * @return Illuminate\Http\Request|Illuminate\Support\Collection|array|string
     */
    public static function getTeamsForFilter()
    {
        // Get the team(s) to report on
        if (\Auth::user()->hasRole('manager')) {
            $teams = \Auth::user()->managedTeams->pluck('name');
            return $teams;
        } else {
            $teams = (array) \Auth::user()->team->name;
            return $teams;
        }
    }

    /**
     * @param $report_array
     * @return array|string|Illuminate\Http\Request|Illuminate\Support\Collection
     */
    public static function generateChartLabels($report_array)
    {
        $labels = [];
        foreach ($report_array->toArray() as $key => $val) {
            $labels[] = $key;
        }
        return $labels;
    }
}