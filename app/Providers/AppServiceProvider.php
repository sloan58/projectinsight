<?php

namespace App\Providers;

use App\Models\User;
use App\Models\Announcement;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
         * Associate all Users to
         * Announcements when created
         */
        Announcement::created(function (Announcement $announcement) {
            $announcement->recipients()->attach(User::all()->pluck('id')->toArray());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local') {
//            $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
            $this->app->register('Barryvdh\Debugbar\ServiceProvider');
        }
    }
}
