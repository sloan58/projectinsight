<?php

namespace App\Traits;

use App\Models\Role;

trait Roleable
{
    /**
     * Relation
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Checking if user has role
     *
     * @param mixed $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    /**
     * Adding a role to the user
     *
     * @param App\Models\Role $role
     */
    public function assignRole($role)
    {
        if (!$this->hasRole($role->name)) {
            $this->roles()->attach($role->id);
        }        
    }

    /**
     * Revoking a role from user
     *
     * @param App\Models\Role $role
     */
    public function revokeRole($role)
    {
        if ($this->hasRole($role->name)) {
            $this->roles()->detach($role->id);
        }
    }

    public function getIsAdminAttribute()
    {
        return $this->hasRole('admin');
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('superadmin');
    }
}