<?php

namespace App\Jobs;

use App\Models\Status;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Project;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class GenerateProjectReportJob implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    /**
     * @var User
     */
    private $user;
    /**
     * @var
     */
    private $projects;
    /**
     * @var
     */
    private $start_date;
    /**
     * @var
     */
    private $end_date;
    /**
     * @var
     */
    private $get_all_dates;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $projects
     * @param $start_date
     * @param $end_date
     * @param $get_all_dates
     * @internal param $projects
     */
    public function __construct(User $user, $projects, $start_date, $end_date, $get_all_dates)
    {
        $this->user = $user;
        $this->projects = $projects;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->get_all_dates = $get_all_dates;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        Log::info('Generate Project Report: Starting GenerateProjectReportJob with start and end dates - ', [$this->start_date, $this->end_date]);

        // Creating the new document...
        $phpWord = new PhpWord();

        // Add Status Update details to the Word doc
        $phpWord = $this->addStatusesTable($phpWord);

        $phpWord = $this->addUserHoursTable($phpWord);

        // Saving the document as OOXML file...
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');

        // The file format takes a Carbon timestamp and replaces spaces and colons with underscores
        $fileNameAndPath = storage_path() . '/Project_Insight-' . implode('_', explode(' ', preg_replace('/:/', '_', Carbon::now()->toDateTimeString()))) . '.docx';
        $objWriter->save($fileNameAndPath);
        Log::info('Generate Project Report: Saved File - ', [$fileNameAndPath]);

        // Mail the report
        Mail::send('emails.project_report', [ ], function($message) use ($fileNameAndPath)
        {
            Log::info('Generate Project Report: Sending email to recipient - ', [$this->user->email]);
            $message->to($this->user->email)->subject("Project Insight Report");
            $message->bcc('martin.sloan@karma-tek.com');
            $message->attach($fileNameAndPath);

        });
    }

    /**
     * @param $phpWord
     */
    private function addStatusesTable($phpWord)
    {
        // Add the main document section
        $section = $phpWord->addSection();

        // Set the Project Header
        $style = ['size' => 12, 'bold' => true];
        $section->addTextBreak(1);
        $report_period = $this->get_all_dates ? 'All Dates' : $this->start_date . ' to ' . $this->end_date;
        $section->addText(htmlspecialchars('Project Report Time Period: ' . $report_period), $style);

        // Iterate each project
        foreach ($this->projects as $project) {

            // Set the Project Header
            Log::debug('Generate Project Report: Working Project - ', [$project]);
            $style = ['size' => 16, 'bold' => true];
            $section->addTextBreak(1);
            $section->addText(htmlspecialchars('Project: ' . $project->name), $style);

            // List the Teams Assigned
            $section->addTextBreak(1);
            $style = ['size' => 10, 'bold' => true];
            $section->addText(htmlspecialchars('Team Assigned: ' . $project->team->name), $style);

            // Get all Project Team members
            $section->addTextBreak(1);
            $style = ['size' => 10, 'bold' => true];
            $team_members = $project->team->members;

            // Add Team Member time contributions for the specific dates
            $section->addText(htmlspecialchars('Team Members (Hours): '), $style);
            foreach($team_members as $member) {
                if($hours = $this->getUserHoursSpentOnProject($project, $member)) {
                    $section->addText(htmlspecialchars($member->username . ': (' . $hours . ')'));
                }
            }
            $section->addTextBreak(1);

            // Create table for Status Update listing
            $section->addTextBreak(1);
            $styleTable = ['borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80];
            $styleFirstRow = ['borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF'];
            $styleCell = ['valign' => 'center'];
            $fontStyle = ['bold' => true, 'align' => 'center'];
            $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
            $table = $section->addTable('Fancy Table');

            // Create table headers
            $table->addRow(600);
            $table->addCell(4000, $styleCell)->addText(htmlspecialchars('Status Updates'), $fontStyle);

            $statuses = $this->getStatuses($project);
            Log::debug('Generate Project Report: Found StatusUpdates - ', [$statuses]);

            // Iterate Project Status Updates
            foreach ($statuses as $status)
            {
                // Add new Word table row
                Log::debug('Generate Project Report: Working StatusUpdate - ', [$status]);
                $table->addRow();
                // Insert the Status Update
                $update = $table->addCell(6000);
                $update->addText(htmlspecialchars('- ' . $status->title . ': ' . $status->body . ' (Task: ' . $status->task->name . ')'));
            }
        }
        return $phpWord;
    }

    /**
     * @param $phpWord
     * @return mixed
     */
    private function addUserHoursTable($phpWord)
    {
        // Add the main document section
        $section = $phpWord->addSection();

        // Set the Project Header
        $style = ['size' => 16, 'bold' => true];
        $section->addTextBreak(1);
        $section->addText(htmlspecialchars('Staff Time Reporting'), $style);

        // Create table for Status Update listing
        $section->addTextBreak(1);
        $styleTable = ['borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80];
        $styleFirstRow = ['borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF'];
        $styleCell = ['valign' => 'center'];
        $fontStyle = ['bold' => true, 'align' => 'center'];
        $phpWord->addTableStyle('Fancy Table', $styleTable, $styleFirstRow);
        $table = $section->addTable('Fancy Table');

        // Create table headers
        $table->addRow(600);
        $table->addCell(4000, $styleCell)->addText(htmlspecialchars('User Name'), $fontStyle);
        $table->addCell(4000, $styleCell)->addText(htmlspecialchars('Hours in Report Period'), $fontStyle);

        // Get all Users and record hours
        $users = User::whereIn('team_id', $this->user->managedTeams->pluck('id')->toArray())->get();

        foreach($users as $user) {
            // Add new Word table row
            $table->addRow();
            // Insert the User Name
            $update = $table->addCell(6000);
            $update->addText(htmlspecialchars($user->username));
            // Insert the Hours
            $update = $table->addCell(6000);
            $update->addText(htmlspecialchars($this->getUserHoursForTimePeriod($user)));
        }
        return $phpWord;
    }

    /**
     * Get the status updates for this Project
     * Either all updates, or between 2 dates
     *
     * @param Project $project
     * @return mixed
     */
    private function getStatuses(Project $project)
    {
        if($this->get_all_dates) {
            return Status::whereIn('task_id', $project->tasks->pluck('id')->toArray())->get();
        } else {
            return Status::whereIn('task_id', $project->tasks->pluck('id')->toArray())->whereBetween('performed_at',[$this->start_date, $this->end_date])->get();
        }
    }

    /**
     * @param Project $project
     * @param User $user
     * @return mixed
     */
    private function getUserHoursSpentOnProject(Project $project, User $user) {
        $hours = '';
        foreach($project->tasks as $task) {
            if($this->get_all_dates) {
                $hours += $task->statuses()->where('user_id', $user->id)->sum('hours_spent');
            } else {
                $hours += $task->statuses()->whereBetween('performed_at', [$this->start_date, $this->end_date])->where('user_id', $user->id)->sum('hours_spent');
            }
        }
        return $hours;
    }

    private function getUserHoursForTimePeriod(User $user)
    {
        if($this->get_all_dates) {
            return $user->statuses->sum('hours_spent');
        } else {
            return $user->statuses()->whereBetween('performed_at', [$this->start_date, $this->end_date])->sum('hours_spent');
        }
    }
}
