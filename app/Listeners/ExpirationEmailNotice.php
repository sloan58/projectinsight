<?php

namespace App\Listeners;

use App\Events\ExpirationUpcoming;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExpirationEmailNotice
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ExpirationUpcoming  $event
     * @return void
     */
    public function handle(ExpirationUpcoming $event)
    {
        Log::debug('Expiration: Starting ExpirationEmailNotice Listener');

        $emailAddresses = $event->expiration->team->members->pluck('email')->toArray();

        // Mail the report
        Log::debug('Expiration: Mailing Team Members', [$emailAddresses]);
        foreach($emailAddresses as $address) {
            Mail::send('emails.upcoming_expiration', [ 'expiration' => $event->expiration ], function($message) use ($address, $event, $emailAddresses)
            {
                $message->to($address)->subject("Project Insight: " . $event->expiration->name . " Expiration upcoming: {$event->expiration->expiration_date->addDay()->diffForHumans()}");
            });   
        }
    }
}
