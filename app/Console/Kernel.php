<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CheckForExpiration::class,
        \App\Console\Commands\SendWeeklyReport::class,
        \App\Console\Commands\StaffWeeklyHoursReport::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
         *  Check for upcoming System Expiration events
         */
        $schedule->command('pi:check-expiration')
                  ->dailyAt('08:00');

        /*
         *  Send automated weekly reporting on Wednesdays at 8:00pm
         */
        $schedule->command('pi:send-weekly-report')
                  ->weekly()
                  ->wednesdays()
                  ->at('20:00');

        /*
         *  Notify Users and their Manager of hours less than 40
         */
        $schedule->command('pi:staff-weekly-hours')
                 ->weekly()
                 ->fridays()
                 ->at('22:00');
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
