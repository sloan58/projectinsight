<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Project;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Jobs\GenerateProjectReportJob;
use Illuminate\Foundation\Bus\DispatchesJobs;

class SendWeeklyReport extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pi:send-weekly-report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send an automated Project Insight Report to subscribed Managers for their staff';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Automated Reporting: Starting SendAutomatedWeeklyReport Command');

        $users = User::whereHas('roles', function($q) {
            $q->where('name','receive_weekly_report');
        })->get();

        Log::debug('Automated Reporting: Found the following users subscribed to weekly reports - ', [$users]);

        $start_date = Carbon::now()->subWeek()->startOfWeek();
        $end_date = Carbon::now()->subWeek()->endOfWeek();
        $get_all_dates = false;
        Log::debug('Automated Reporting: Set report start and end parameters', [$start_date, $end_date]);

        foreach($users as $user) {
            $projects = Project::whereIn('team_id', $user->managedTeams->pluck('id')->toArray())->get();
            Log::debug('Automated Reporting: Dispatching GenerateProjectReportJob for User - ', [$user]);
            $this->dispatch(new GenerateProjectReportJob($user, $projects, $start_date, $end_date, $get_all_dates));
        }
    }
}
