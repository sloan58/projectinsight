<?php

namespace App\Console\Commands;

use App\Models\Team;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class StaffWeeklyHoursReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pi:staff-weekly-hours';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send staff an email reminder of their hours if under 40 for the week';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::info('Staff Weekly Hours Report: Starting StaffWeeklyHoursReport Command');

        $team = Team::where('name', 'NIPT Engineering')->first();

        $notifiedMembers = [];
        foreach($team->members as $member) {

            // Get hours submitted this week
            $hours = $member->hoursThisWeek();

            // If their hours are under 40, email them to please update.
            if($hours < 40) {
                // Mail the report
                Mail::send('emails.user_weekly_hours_report', [ 'member' => $member, 'hours' => $hours ], function($message) use ($member)
                {
                    Log::info('Staff Weekly Hours Report: Sending email to recipient - ', [$member->email]);
                    $message->to($member->email)->subject($member->username . " - Project Insight Weekly Hours Notice");
                    $message->bcc('martin.sloan@karma-tek.com');
                });
                $notifiedMembers[] = $member->username . ' with ' . $hours . ' hours.';
            }
        }


        // Notify the managers
        $managers = $team->managers;
        foreach($managers as $manager) {

            Mail::send('emails.manager_weekly_hours_report', ['notifiedMembers' => $notifiedMembers ], function($message) use ($manager)
            {
                Log::info('Staff Weekly Hours Notice Manager Summary: Sending email to recipient - ', [$manager->email]);
                $message->to($manager->email)->subject($manager->username . " - Project Insight Weekly Hours Notice Manager Summary");
                $message->bcc('martin.sloan@karma-tek.com');
            });
        }
    }
}
