<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Models\Expiration;
use Illuminate\Console\Command;
use App\Events\ExpirationUpcoming;
use Illuminate\Support\Facades\Log;

class CheckForExpiration extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pi:check-expiration';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check for upcoming Expiration Events';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::debug('Expiration: Starting CheckForExpiration Command');
        $expirations = Expiration::all();

        foreach($expirations as $expiration) {
            if($expiration->expiration_date->toDateString() == Carbon::now()->addMonths(3)->toDateString()) {
                // Fire ExpirationUpcoming Event
                Log::debug('System Expiration: Found Expiration Upcoming in 90 Days', [$expiration]);
                event(new ExpirationUpcoming($expiration));
            } elseif($expiration->expiration_date->toDateString() == Carbon::now()->addMonths(2)->toDateString()) {
                // Fire ExpirationUpcoming Event
                Log::debug('System Expiration: Found Expiration Upcoming in 60 Days', [$expiration]);
                event(new ExpirationUpcoming($expiration));
            } elseif ($expiration->expiration_date->toDateString() == Carbon::now()->addMonth()->toDateString()) {
                // Fire ExpirationUpcoming Event
                Log::debug('System Expiration: Found Expiration Upcoming in 30 Days', [$expiration]);
                event(new ExpirationUpcoming($expiration));
            } elseif($expiration->expiration_date->toDateString() == Carbon::now()->addDays(15)->toDateString()) {
                // Fire ExpirationUpcoming Event
                Log::debug('System Expiration: Found System Expiration Upcoming in 15 Days', [$expiration]);
                event(new ExpirationUpcoming($expiration));
            } elseif($expiration->expiration_date->toDateString() == Carbon::now()->addDay()->toDateString()) {
                // Fire ExpirationUpcoming Event
                Log::debug('System Expiration: Found System Expiration Upcoming tomorrow', [$expiration]);
                event(new ExpirationUpcoming($expiration));
                // If this is a recurring event, reschedule it.
                if($expiration->recurring) {
                    Log::debug('System Expiration: This is a recurring event.  Moving the expiration out 89 days.', [$expiration->expiration_date->addDays(89)]);
                    $expiration->expiration_date->addDays(89);
                    $expiration->save();
                }
            }
        }

        Log::debug('Expiration: Completed CheckForExpiration Command');
    }
}
