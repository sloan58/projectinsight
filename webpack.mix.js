let mix = require('laravel-mix');

mix.copy('node_modules/font-awesome/fonts', 'public/fonts')
    .copy('node_modules/es6-object-assign/dist/object-assign.min.js', 'public/js')
    .copy('resources/assets/fonts', 'public/fonts')
    .copy('node_modules/bootstrap-sass/assets/fonts/bootstrap', 'public/css/fonts')
    .copy('resources/assets/img', 'public/img');
mix.js('resources/assets/js/app.js', 'public/js');
mix.sass('resources/assets/sass/app.scss', 'public/css');
